<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class Eliminar_FeriadoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ID_USUARIO' => 'required|integer|exists:USUARIO,ID',
            'ID_FERIADO' => 'required|integer|exists:FERIADOS,ID'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $arrayErrores = $validator->failed();
        $integer_i = 0; 
        $object_response = array();

        if (is_array(@$arrayErrores['ID_USUARIO'])){

            if (is_array(@$arrayErrores['ID_USUARIO']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un ID de Usuario";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['ID_USUARIO']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El ID de Usuario debe ser numerico";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['ID_USUARIO']['Exists'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "No se encontro usuario con ID entregado";
                $integer_i++;
            } 
        }                                
        if (is_array(@$arrayErrores['ID_FERIADO'])){

            if (is_array(@$arrayErrores['ID_FERIADO']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Feriado invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un ID de Feriado";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['ID_FERIADO']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Feriado invalido';
                $object_response['errors'][$integer_i]['detail'] = "El ID de Feriado debe ser numerico";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['ID_FERIADO']['Exists'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Feriado invalido';
                $object_response['errors'][$integer_i]['detail'] = "No se encontro feriado con ID entregado";
                $integer_i++;
            } 
        }  
        throw new HttpResponseException(response()->json($object_response, 400));
    }
}
