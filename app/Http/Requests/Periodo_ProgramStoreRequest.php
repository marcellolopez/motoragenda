<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Carbon\Carbon;
class Periodo_ProgramStoreRequest extends FormRequest
{

   /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ESTABLECIMIENTO_ID' => ['required','integer','exists:ESTABLECIMIENTO,ID'],
            'FECHA_DESDE' => ['required','date_format:Y/m/d','after:today'],
            'FECHA_HASTA' => ['required','date_format:Y/m/d','after:today',function($attribute, $value, $fail){
                /**
                 * Creacion de regla custom
                 * --diferencia de dias entre fechas
                 */
                $hasta = Carbon::parse($this->FECHA_HASTA);
                $desde =  Carbon::parse($this->FECHA_DESDE); 
                $diff_in_days = $hasta->diffInDays($desde);
                if ($diff_in_days > 364){
                    $fail($attribute.' is invalid.');
                }
            }],
            'ACTIVO' => ['in:S,N','nullable'],
            'USUARIO_ID_MOD' => ['required','integer','exists:USUARIO,ID']
        ];
    }
    

    protected function failedValidation(Validator $validator)
    {
        //echo "test";
        $arrayErrores = $validator->failed();
        $integer_i = 0; 
        $object_response = array();
        //var_dump($arrayErrores);
       
        
        if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID'])){
            if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Id de establecimiento invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un ID de Establecimiento";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Id de establecimiento invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo 'ESTABLECIMIENTO_ID' debe ser numerico";
                $integer_i++;
            }
            if (@$arrayErrores['ESTABLECIMIENTO_ID']['Exists']){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Id de establecimiento invalido';
                $object_response['errors'][$integer_i]['detail'] = "No se encontro el establecimiendo";
                $integer_i++;
            } 

        }
        if (is_array(@$arrayErrores['FECHA_DESDE'])){
            if (is_array(@$arrayErrores['FECHA_DESDE']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar una Fecha";
                $integer_i++;
            }            
            if (is_array(@$arrayErrores['FECHA_DESDE']['DateFormat'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_desde no es tipo fecha o no cumple el formato 'Y/m/d'";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['FECHA_DESDE']['After'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_desde debe ser mayor a la fecha de hoy'";
                $integer_i++;
            }
        }
        if (is_array(@$arrayErrores['FECHA_HASTA'])){
            if (is_array(@$arrayErrores['FECHA_HASTA']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar una Fecha";
                $integer_i++;
            }            
            if (is_array(@$arrayErrores['FECHA_HASTA']['DateFormat'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_hasta no es tipo fecha o no cumple el formato 'Y/m/d'";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['FECHA_HASTA']['After'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_hasta debe ser mayor a la fecha de hoy'";
                $integer_i++;
            }            
            if (is_array(@$arrayErrores['FECHA_HASTA']['Illuminate\Validation\ClosureValidationRule'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_hasta no puede ser mayor que un año de Fecha_desde'";
                $integer_i++;
            }
        }
        if (is_array(@$arrayErrores['ACTIVO'])){            
            if (is_array(@$arrayErrores['ACTIVO']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Activo invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un estado para Activo";
                $integer_i++;
            }  
            if (is_array(@$arrayErrores['ACTIVO']['In'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Activo invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Activo debe ser 'S' o 'N'";
                $integer_i++;
            }  

        }
        if (is_array(@$arrayErrores['USUARIO_ID_MOD'])){

            if (is_array(@$arrayErrores['USUARIO_ID_MOD']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un ID de Usuario";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['USUARIO_ID_MOD']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El ID de Usuario debe ser numerico";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['USUARIO_ID_MOD']['Exists'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'Periodo_ProgramController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "No se encontro usuario con ID entregado";
                $integer_i++;
            } 
        }                        
        throw new HttpResponseException(response()->json($object_response, 400));
    }
}
