<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 11/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Helpers\Format;

class CrearBloqueHorarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /** 
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'CONTRATO_ID' => 'required|integer|exists:CONTRATO,ID',
            'JERARQUIA_ESPEC_ID' => 'required|numeric|exists:JERARQUIA_ESPEC,ID',
            'DIA' => 'required|integer|between:1,6', 
            'TIEMPO_X_PACIENTE' => 'integer|nullable',
            'CANTIDAD_PACIENTES' => 'integer|nullable',
            'HORA_INICIO' => 'required|date_format:H:i',
            'HORA_FIN' => 'required|date_format:H:i|after:HORA_INICIO',
            'TIPO_BLOQUE_ID' => 'required|exists:TIPO_BLOQUE,ID',
            'ACTIVO' => 'in:S,N|nullable',
            'USUARIO_MOD_ID' => 'required|integer|exists:USUARIO,ID',
            'AUSENCIAS_ID' => 'integer|exists:AUSENCIAS,ID|nullable'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $array_errores = $validator->failed();
        //var_dump($array_errores);
        //exit;
        $integer_i = 0; 
        $object_response = array();        
        foreach ($array_errores as $campo => $errores) {            
            foreach ($errores as $tipo_error => $arreglo) {
                if(@$arreglo[0]){
                    $referencia = $arreglo[0];
                    if(@$arreglo[1]){
                        $rango = $arreglo[1];
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'BloquehorarioController@store', $tipo_error, $referencia, $rango);
                    }else{
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'BloquehorarioController@store', $tipo_error, $referencia);
                    }
                }else{
                    $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'BloquehorarioController@store', $tipo_error);
                }
                
                $integer_i++;
            }
        }
        throw new HttpResponseException(response()->json($object_response, 400));

    }


}
