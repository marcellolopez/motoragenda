<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 18/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Helpers\Format;
use Illuminate\Support\Facades\DB;

class ListarSobrecuposPacientesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'RUT_PACIENTE' => 'required|integer|exists:DATOS_PACIENTE,RUT',
            'ESTABLECIMIENTO_ID' => 'required|integer|exists:ESTABLECIMIENTO,ID',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $array_errores = $validator->failed();
        //var_dump($array_errores);
        //exit;
        $integer_i = 0; 
        $object_response = array();        
        foreach ($array_errores as $campo => $errores) {            
            foreach ($errores as $tipo_error => $arreglo) {
                if(@$arreglo[0]){
                    $referencia = $arreglo[0];
                    if(@$arreglo[1]){
                        $rango = $arreglo[1];
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'SobreCupoController@listadoSobrecuposPaciente', $tipo_error, $referencia, $rango);
                    }else{
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'SobreCupoController@listadoSobrecuposPaciente', $tipo_error, $referencia);
                    }
                }else{
                    $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'SobreCupoController@listadoSobrecuposPaciente', $tipo_error);
                }
                
                $integer_i++;
            }
        }
        throw new HttpResponseException(response()->json($object_response, 400));

    }


}
