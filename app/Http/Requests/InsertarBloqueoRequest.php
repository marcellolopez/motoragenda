<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 17/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Helpers\Format;

class InsertarBloqueoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'FECHA_DESDE'        => 'required|date|before_or_equal:FECHA_HASTA',
            'FECHA_HASTA'        => 'required|date|after_or_equal:FECHA_DESDE',
            'CONTRATO_ID'        => 'required|integer|exists:CONTRATO,ID',
            'MOTIVO_AUSENCIA_ID' => 'required|integer|exists:MOTIVO_AUSENCIA,ID',
            'DESTINO_PACIENT_ID' => 'required|integer|exists:DESTINO_PACIENTE,ID',
            'SOLICITANT_BLOQ_ID' => 'required|integer|exists:SOLICIT_BLOQUEO,ID',
            'OBSERVACION'        => 'required|max:500',
            'USUARIO_ID'         => 'required|exists:USUARIO,ID',
            'BLOQUE_HORARIO_ID'  => 'required|integer|exists:BLOQUE_HORARIO,ID',
            'IP'                 => 'required|ipv4'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $array_errores = $validator->failed();
        $integer_i = 0; 
        $object_response = array();        
        foreach ($array_errores as $campo => $errores) {            
            foreach ($errores as $tipo_error => $arreglo) {
                if(@$arreglo[0]){
                    $referencia = $arreglo[0];
                    if(@$arreglo[1]){
                        $rango = $arreglo[1];
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'BloquearAgendaController', $tipo_error, $referencia, $rango);
                    }else{
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'BloquearAgendaController', $tipo_error, $referencia);
                    }
                }else{
                    $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'BloquearAgendaController', $tipo_error);
                }
                
                $integer_i++;
            }
        }
        throw new HttpResponseException(response()->json($object_response, 400));

    }    
}
