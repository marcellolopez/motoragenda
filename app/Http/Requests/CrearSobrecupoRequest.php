<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 18/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Helpers\Format;
use Illuminate\Support\Facades\DB;

class CrearSobrecupoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'BLOQUE_HORARIO_ID' => 'required|integer|exists:BLOQUE_HORARIO,ID',
            'TIPO_ATENCION_ID' => 'required|integer|exists:ATENCION_ESTAB,TIPO_ATENCION_ID',
            'RUT_PACIENTE' => 'required|integer|exists:DATOS_PACIENTE,RUT',
            'HORA' => 'required|date_format:H:i',
            'FECHA_SC' => 'required|date_format:Y/m/d|after:today',
            'IC_ID' => ['integer',function($attribute, $value, $fail){
                /**
                 * Creacion de regla custom
                 * --verificar existencia IC_ID
                 */

                    $results = DB::select( DB::raw(
                        "SELECT ID
                            FROM DBGES.INTERCONSULTA IC
                            WHERE IC.ID = :id"), 
                    array(
                        'id' => $value
                    ));
                    
                    if ($results == null){
                        $fail($attribute.' is invalid.');
                    }
                
                
                },'nullable'],  //dbges.INTERCONSULTA
            'OA_ID' => ['integer',function($attribute, $value, $fail){
                /**
                 * Creacion de regla custom
                 * --verificar existencia OA_ID
                 */
                  
                    $results = DB::select( DB::raw(
                        "SELECT ID
                            FROM INTERCONSULTA.INTERCONSULTA IC
                            WHERE IC.ID = :id"), 
                    array(
                        'id' => $value
                    ));
                    
                    if ($results == null){
                        $fail($attribute.' is invalid.');
                    }
                },'nullable'], //INTERCONSULTA.INTERCONSULTA  
            'USUARIO_CREA_ID' => 'required|integer|exists:USUARIO,ID',
            'IP_CREA' => 'required|ipv4'
            
            /*'DIA' => 'required|integer|between:1,6', 
            'TIEMPO_X_PACIENTE' => 'integer|nullable',
            'CANTIDAD_PACIENTES' => 'integer|nullable',
            'HORA' => 'required|date_format:H:i',
            'HORA_FIN' => 'required|date_format:H:i|after:HORA_INICIO',
            'TIPO_BLOQUE_ID' => 'required|exists:TIPO_BLOQUE,ID',
            'ACTIVO' => 'in:S,N|nullable',
            'USUARIO_MOD_ID' => 'required|integer|exists:USUARIO,ID',
            'AUSENCIAS_ID' => 'integer|exists:AUSENCIAS,ID|nullable'*/
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $array_errores = $validator->failed();
        //var_dump($array_errores);
        //exit;
        $integer_i = 0; 
        $object_response = array();        
        foreach ($array_errores as $campo => $errores) {            
            foreach ($errores as $tipo_error => $arreglo) {
                if(@$arreglo[0]){
                    $referencia = $arreglo[0];
                    if(@$arreglo[1]){
                        $rango = $arreglo[1];
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'SobreCupoController@store', $tipo_error, $referencia, $rango);
                    }else{
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'SobreCupoController@store', $tipo_error, $referencia);
                    }
                }else{
                    $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'SobreCupoController@store', $tipo_error);
                }
                
                $integer_i++;
            }
        }
        throw new HttpResponseException(response()->json($object_response, 400));

    }


}
