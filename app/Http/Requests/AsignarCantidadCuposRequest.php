<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Helpers\Format;


class AsignarCantidadCuposRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ESTABLECIMIENTO_ID' => 'required|integer|exists:ESTABLECIMIENTO,ID',
            'TIPO_ATENCION_ID' => 'required|integer|exists:TIPO_ATENCION,ID',
            'BLOQUE_HORARIO_ID' => 'required|integer|exists:BLOQUE_HORARIO,ID',            
            'USUARIO_CREA_ID' => 'required|integer|exists:USUARIO,ID',
            'CANTIDAD' => 'required|integer',
            'IP_CREA' => 'required|ipv4'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $array_errores = $validator->failed();
        $integer_i = 0; 
        $object_response = array();        
        foreach ($array_errores as $campo => $errores) {            
            foreach ($errores as $tipo_error => $arreglo) {
                if(@$arreglo[0]){
                    $referencia = $arreglo[0];
                    if(@$arreglo[1]){
                        $rango = $arreglo[1];
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'DetallarAtencionController@asignar_cantidad_cupos', $tipo_error, $referencia, $rango);
                    }else{
                        $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'DetallarAtencionController@asignar_cantidad_cupos', $tipo_error, $referencia);
                    }
                }else{
                    $object_response['errors'][$integer_i] = Format::failedObjectResponse($campo, 'DetallarAtencionController@asignar_cantidad_cupos', $tipo_error);
                }
                
                $integer_i++;
            }
        }
        throw new HttpResponseException(response()->json($object_response, 400));
    }

}
