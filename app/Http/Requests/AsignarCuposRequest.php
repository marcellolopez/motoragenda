<?php
/** 
 * Desarrollador: 
 * 02/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AsignarCuposRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'RUT' => 'required|integer|min:7|max:8',
            'DV'  => 'required|integer|max:1'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $arrayErrores = $validator->failed();
        $integer_i = 0; 
        $object_response = array();

        if (is_array(@$arrayErrores['RUT'])){

            if (is_array(@$arrayErrores['RUT']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'RUT Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un RUT de Usuario";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['RUT']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'RUT Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El RUT de Usuario debe ser numerico";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['RUT']['min'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'RUT Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El RUT de Usuario debe contener 7 caracteres como mínimo";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['RUT']['max'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'RUT Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El RUT de Usuario debe contener 8 caracteres como maximo";
                $integer_i++;
            }
        }                                
        if (is_array(@$arrayErrores['DV'])){

            if (is_array(@$arrayErrores['DV']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'DV invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar el DV";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['DV']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'DV invalido';
                $object_response['errors'][$integer_i]['detail'] = "El DV debe ser numerico";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['RUT']['max'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][$integer_i]['title'] = 'RUT Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El RUT de Usuario debe contener 1 caracter como maximo";
                $integer_i++;
            }

        }

        //throw new HttpResponseException(response()->json($object_response, 400));

    }
}
