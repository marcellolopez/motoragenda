<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class Listar_ProgramacionStoreRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ESTABLECIMIENTO_ID' => 'required|integer|min:1|exists:ESTABLECIMIENTO,ID'
        ];
    }
    

    protected function failedValidation(Validator $validator)
    {

        $arrayErrores = $validator->failed();

        if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID']['Required'])){
            $string_detalle = "Debe ingresar un ID";
        }
        if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID']['Integer'])){
            $string_detalle =  "Campo 'ESTABLECIMIENTO_ID' debe ser numerico";
        }
        if (@$arrayErrores['ESTABLECIMIENTO_ID']['Min']){
            $string_detalle = "Campo 'ESTABLECIMIENTO_ID' debe ser mayor que 1";
        }    
        if (@$arrayErrores['ESTABLECIMIENTO_ID']['Exists']){
            $string_detalle = "No se encontro el establecimiendo";
        } 

        $object_response['errors'][0]['status'] = 400;
        $object_response['errors'][0]['source'] = 'Periodo_ProgramController@index';
        $object_response['errors'][0]['title'] = 'Id de establecimiento invalido';
        $object_response['errors'][0]['detail'] = $string_detalle;

        throw new HttpResponseException(response()->json($object_response, 400));
    }
}
