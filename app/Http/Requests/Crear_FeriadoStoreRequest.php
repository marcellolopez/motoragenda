<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class Crear_FeriadoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ESTABLECIMIENTO_ID' => 'required|integer|exists:ESTABLECIMIENTO,ID',
            'FECHA_DESDE' => 'required|date_format:Y/m/d|after:today', //|exists:FERIADOS,FECHA_DESDE
            'FECHA_HASTA' => 'required|date_format:Y/m/d|after:today|date_equals:FECHA_DESDE',//|exists:FERIADOS,FECHA_HASTA
            'ACTIVO' => 'in:S,N|nullable',
            'USUARIO_CREA_ID' => 'required|integer|exists:USUARIO,ID',
            'IP_CREA' => 'required|ipv4'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $arrayErrores = $validator->failed();
        $integer_i = 0; 
        $object_response = array();
        if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID'])){
            if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Id de establecimiento invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un ID de Establecimiento";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['ESTABLECIMIENTO_ID']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Id de establecimiento invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo 'ESTABLECIMIENTO_ID' debe ser numerico";
                $integer_i++;
            }
            if (@$arrayErrores['ESTABLECIMIENTO_ID']['Exists']){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Id de establecimiento invalido';
                $object_response['errors'][$integer_i]['detail'] = "No se encontro el establecimiendo";
                $integer_i++;
            } 

        }
        if (is_array(@$arrayErrores['FECHA_DESDE'])){
            if (is_array(@$arrayErrores['FECHA_DESDE']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar una Fecha";
                $integer_i++;
            }            
            if (is_array(@$arrayErrores['FECHA_DESDE']['DateFormat'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_desde no es tipo fecha o no cumple el formato 'Y/m/d'";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['FECHA_DESDE']['After'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_desde debe ser mayor a la fecha de hoy'";
                $integer_i++;
            }            
            /*if (is_array(@$arrayErrores['FECHA_DESDE']['Exists'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_desde ya existe en el sistema'";
                $integer_i++;
            }*/
        }
        if (is_array(@$arrayErrores['FECHA_HASTA'])){
            if (is_array(@$arrayErrores['FECHA_HASTA']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar una Fecha";
                $integer_i++;
            }            
            if (is_array(@$arrayErrores['FECHA_HASTA']['DateFormat'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_hasta no es tipo fecha o no cumple el formato 'Y/m/d'";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['FECHA_HASTA']['After'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_hasta debe ser mayor a la fecha de hoy'";
                $integer_i++;
            }            
            if (is_array(@$arrayErrores['FECHA_HASTA']['DateEquals'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_hasta invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_hasta debe igual a campo Fecha_desde'";
                $integer_i++;
            }
            /*if (is_array(@$arrayErrores['FECHA_HASTA']['Exists'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Fecha_desde invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Fecha_desde ya existe en el sistema'";
                $integer_i++;
            }*/
        }
        if (is_array(@$arrayErrores['ACTIVO'])){            
            if (is_array(@$arrayErrores['ACTIVO']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Activo invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un estado para Activo";
                $integer_i++;
            }  
            if (is_array(@$arrayErrores['ACTIVO']['In'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'Campo Activo invalido';
                $object_response['errors'][$integer_i]['detail'] = "Campo Activo debe ser 'S' o 'N'";
                $integer_i++;
            }  

        }
        if (is_array(@$arrayErrores['USUARIO_CREA_ID'])){

            if (is_array(@$arrayErrores['USUARIO_CREA_ID']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar un ID de Usuario";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['USUARIO_CREA_ID']['Integer'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "El ID de Usuario debe ser numerico";
                $integer_i++;
            }
            if (is_array(@$arrayErrores['USUARIO_CREA_ID']['Exists'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'ID Usuario modificador invalido';
                $object_response['errors'][$integer_i]['detail'] = "No se encontro usuario con ID entregado";
                $integer_i++;
            } 
        }                                
        if (is_array(@$arrayErrores['IP_CREA'])){

            if (is_array(@$arrayErrores['IP_CREA']['Required'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'IP invalida';
                $object_response['errors'][$integer_i]['detail'] = "Debe ingresar la dirección IP de origen";
                $integer_i++;
            }       
            if (is_array(@$arrayErrores['IP_CREA']['Ipv4'])){
                $object_response['errors'][$integer_i]['status'] = 400;
                $object_response['errors'][$integer_i]['source'] = 'FeriadoController@store';
                $object_response['errors'][$integer_i]['title'] = 'IP invalida';
                $object_response['errors'][$integer_i]['detail'] = "El valor ingresado no corresponde a una direccion IPv4 valida";
                $integer_i++;
            }  
        }
        throw new HttpResponseException(response()->json($object_response, 400));
    }
}
