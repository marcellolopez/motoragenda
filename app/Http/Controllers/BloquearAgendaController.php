<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Carbon\Carbon;
use App\Http\Controllers\Helpers\Format;


use App\Entities\Persona;
use App\Entities\Contrato;
use App\Entities\BloqueHorario;
use App\Entities\JerarquiaEspec;
use App\Entities\Ausencia;

use App\Http\Requests\ObtenerContratosProfesionalRequest;
use App\Http\Requests\ObtenerMotivosAusenciaRequest;
use App\Http\Requests\InsertarBloqueoRequest;


class BloquearAgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ObtenerContratosProfesionalRequest $request)
    {
        try {
            $response_prof = Persona::where('RUT', $request->RUT)->first();
            $schema = $request->get('SCHEMA');



            $response_contratos =   DB::table( $schema.'.CONTRATO')
                ->select(
                    'CONTRATO.ID as id',
                    'CARGOS.DESCRIPCION as cargo'
                )
                ->where('CONTRATO.PERSONA_ID', $response_prof->id)
                ->where('CONTRATO.ESTABLECIMIENTO_ID', $request->ESTABLECIMIENTO_ID)
                ->where('CONTRATO.ACTIVO', 'S')
                ->join('CARGOS', 'CONTRATO.CARGO_ID', '=', 'CARGOS.ID')
                ->orderBy('CONTRATO.ID', 'ASC')
                ->get();

            if (!$response_contratos) {
                $object_response['data'][0]['type'] = "Contratos del Profesional";
                $object_response['data'][0]['status'] = false;
                $object_response['data'][0]['message'] = "Profesional no posee contratos con este establecimiento";
            }
            else{

                //agregar info persona a json
                $object_response['data'][0]['type'] = "Profesional (Persona)";
                $object_response['data'][0]['id_persona'] = $response_prof->id;
                $object_response['data'][0]['attributes']['rut'] = $response_prof->rut;
                $object_response['data'][0]['attributes']['dv'] = $response_prof->dv;
                $object_response['data'][0]['attributes']['nombres'] = $response_prof->nombres;
                $object_response['data'][0]['attributes']['apellido_pat'] = $response_prof->apellido_pat;
                $object_response['data'][0]['attributes']['apellido_mat'] = $response_prof->apellido_mat;
                $object_response['data'][0]['attributes']['sexo_id'] = $response_prof->sexo_id;
                $object_response['data'][0]['attributes']['direccion'] = $response_prof->direccion;
                $object_response['data'][0]['attributes']['estado_civil_id'] = $response_prof->estado_civil_id;
                $object_response['data'][0]['attributes']['comunas_id'] = $response_prof->comunas_id;
                $object_response['data'][0]['attributes']['fono_casa'] = $response_prof->fono_casa;
                $object_response['data'][0]['attributes']['fono_oficina'] = $response_prof->fono_oficina;
                $object_response['data'][0]['attributes']['fono_movil'] = $response_prof->fono_movil;
                $object_response['data'][0]['attributes']['email'] = $response_prof->email;
                $object_response['data'][0]['attributes']['activo'] = $response_prof->activo;
                $object_response['data'][0]['attributes']['usuario_mod_id'] = $response_prof->usuario_mod_id;
                $object_response['data'][0]['attributes']['fecha_mod'] = $response_prof->fecha_mod;
                $object_response['data'][0]['attributes']['imei1'] = $response_prof->imei1;
                $object_response['data'][0]['attributes']['imei2'] = $response_prof->imei2;
                $object_response['data'][0]['attributes']['desde_ins'] = $response_prof->desde_ins;
                $object_response['data'][0]['attributes']['validado_api_minsal'] = $response_prof->validado_api_minsal;
                $object_response['data'][0]['attributes']['fecha_nacimiento'] = $response_prof->fecha_nacimiento;    
                            
                foreach ($response_contratos as $key => $contrato) {
                    
                    //Agrega los contratos asociados al profesional
                    $object_response['data'][0]['contratos'][$key]['type'] = "Contrato del profesional";
                    $object_response['data'][0]['contratos'][$key]['contrato_id'] = $contrato->id;
                    $object_response['data'][0]['contratos'][$key]['cargo'] = $contrato->cargo;

                }
            
            }
            throw new HttpResponseException(response()->json($object_response, 200));  
           

        } catch (Exception $e) {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));           
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function obtenerMotivosAusencia(ObtenerMotivosAusenciaRequest $request)
    {
        try {
            $response_prof = Persona::where('RUT', $request->RUT)->first();     
            
            $response_contratos = Contrato::with('ausencia', 'motivoausencia')
                ->where('id',$request->CONTRATO_ID)
                ->where('ACTIVO', 'S')
                ->orderBy('ID', 'ASC')
                ->first();

            $ausencias = Ausencia::with('motivoausencia', 'destinopaciente', 'solicitudbloqueo')
                ->where('CONTRATO_ID',$request->CONTRATO_ID)
                ->where('ACTIVO', 'S')
                ->orderBy('ID', 'ASC')
                ->get();

            if (!$response_contratos) {
                $object_response['data'][0]['type'] = "Contratos del Profesional";
                $object_response['data'][0]['status'] = false;
                $object_response['data'][0]['message'] = "Profesional no posee contratos con este establecimiento";            
                throw new HttpResponseException(response()->json($object_response, 402));
            }
            elseif (!$ausencias) {
                $object_response['data'][0]['type'] = "Ausencias";
                $object_response['data'][0]['status'] = false;
                $object_response['data'][0]['message'] = "Profesional no posee ausencias con este establecimiento";            
                throw new HttpResponseException(response()->json($object_response, 402));
            }
            else{

                foreach ($ausencias as $key => $ausencias) {
                    $object_response['data'][$key]['type'] = "Ausencia";
                    $object_response['data'][$key]['attributes']['fecha'] = $ausencias->fecha;
                    $object_response['data'][$key]['attributes']['fecha_desde'] = $ausencias->fecha_desde;
                    $object_response['data'][$key]['attributes']['fecha_hasta'] = $ausencias->fecha_hasta;
                    $object_response['data'][$key]['attributes']['observacion'] = $ausencias->observacion;
                    $object_response['data'][$key]['attributes']['fecha_nacimiento'] = $ausencias->fecha_nacimiento;    

                    $object_response['data'][$key]['motivo_ausencia']['type'] = "Motivo ausencia";
                    $object_response['data'][$key]['motivo_ausencia']['id'] = $ausencias->motivoausencia->id;
                    $object_response['data'][$key]['motivo_ausencia']['codigo'] = $ausencias->motivoausencia->codigo;
                    $object_response['data'][$key]['motivo_ausencia']['descripcion'] = $ausencias->motivoausencia->descripcion;

                    $object_response['data'][$key]['destino_paciente']['type'] = "Destino paciente";
                    $object_response['data'][$key]['destino_paciente']['id'] = $ausencias->destinopaciente->id;
                    $object_response['data'][$key]['destino_paciente']['codigo'] = $ausencias->destinopaciente->codigo;
                    $object_response['data'][$key]['destino_paciente']['descripcion'] = $ausencias->destinopaciente->descripcion;

                    $object_response['data'][$key]['solicitud_bloqueo']['type'] = "Solicitud bloqueo";
                    $object_response['data'][$key]['solicitud_bloqueo']['id'] = $ausencias->solicitudbloqueo->id;
                    $object_response['data'][$key]['solicitud_bloqueo']['codigo'] = $ausencias->solicitudbloqueo->codigo;
                    $object_response['data'][$key]['solicitud_bloqueo']['descripcion'] = $ausencias->solicitudbloqueo->descripcion;
                }


            }
            throw new HttpResponseException(response()->json($object_response, 200));  
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }        

    }

    public function insertarBloqueo(InsertarBloqueoRequest $request)
    {
        try{
            $fecha_desde = $request->get('FECHA_DESDE');
            $fecha_hasta = $request->get('FECHA_HASTA');  
            $contrato_id = $request->get('CONTRATO_ID');       
            
            $cantidad_pacientes = BloqueHorario::where('BLOQUE_HORARIO.CONTRATO_ID', $contrato_id)
                ->whereBetween('HORAS_GENERADAS.FECHA', [ date("Y-m-d H:i:s", strtotime($fecha_desde)), date("Y-m-d H:i:s", strtotime($fecha_hasta))])
                ->join('HORAS_GENERADAS', 'BLOQUE_HORARIO.ID', '=', 'HORAS_GENERADAS.BLOQUE_HORARIO_ID')   
                ->count(); 

            $ausencia = Ausencia::create([
                'CONTRATO_ID' => $request->get('CONTRATO_ID'),
                'FECHA' => now(),
                'FECHA_DESDE' => $request->get('FECHA_DESDE'),
                'FECHA_HASTA' => $request->get('FECHA_HASTA'),
                'MOTIVO_AUSENCIA_ID' => $request->get('MOTIVO_AUSENCIA_ID'),
                'CANTIDAD_PACIENTES' => $cantidad_pacientes,
                'DESTINO_PACIENT_ID' => $request->get('DESTINO_PACIENT_ID'),
                'SOLICITANT_BLOQ_ID' => $request->get('SOLICITANT_BLOQ_ID'),
                'OBSERVACION' => $request->get('OBSERVACION'),
                'ACTIVO' => 'S',
                'USUARIO_ID_MOD' => $request->get('USUARIO_ID'),
                'FECHA_MOD' => now(),
                'BLOQUE_HORARIO_ID' => $request->get('BLOQUE_HORARIO_ID'),
                'USUARIO_CREA_ID' => $request->get('USUARIO_ID'),
                'FECHA_CREA' => now(),
                'USUARIO_MOD_ID' => $request->get('USUARIO_ID'),
                'IP_CREA' => $request->get('IP'),
                'IP_MOD' => $request->get('IP')
            ]);    

            if($ausencia)
            {
                $object_response["success"] = true;
                $object_response["message"] = "Bloqueo ingresado con exito";
                $object_response["data"][0]['id'] = $resp_fer->id;            
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }                
    }    

    public function verificarBloqueo(Request $request)
    {
        $fecha_desde = date("Y-m-d H:i:s", strtotime($request->FECHA_DESDE));
        $fecha_hasta = date("Y-m-d H:i:s", strtotime($request->FECHA_HASTA));

        $bloqueos = DB::table('AUSENCIAS')   
            ->select(
                'AUSENCIAS.ID as id',
                'AUSENCIAS.FECHA_DESDE as fecha_desde',
                'AUSENCIAS.FECHA_HASTA as fecha_hasta',
                'MOTIVO_AUSENCIA.ID as motivo_ausencia_id',
                'BLOQUE_HORARIO.ID as bloque_horario_id'
            )            
            ->where(function($query) use ($fecha_desde,$fecha_hasta){
                 $query->where('AUSENCIAS.FECHA_DESDE', '>=', $fecha_desde);
                 $query->where('AUSENCIAS.FECHA_DESDE', '<=', $fecha_hasta);
             })
             ->orWhere(function($query) use ($fecha_desde,$fecha_hasta){
                 $query->where('AUSENCIAS.FECHA_HASTA', '>=', $fecha_desde);
                 $query->where('AUSENCIAS.FECHA_HASTA', '<=', $fecha_hasta);
             })
             ->orWhere(function($query) use ($fecha_desde,$fecha_hasta){
                 $query->where('AUSENCIAS.FECHA_DESDE', '<=', $fecha_desde);
                 $query->where('AUSENCIAS.FECHA_HASTA', '>=', $fecha_hasta);
             })
            ->join('MOTIVO_AUSENCIA', 'AUSENCIAS.MOTIVO_AUSENCIA_ID', '=', 'MOTIVO_AUSENCIA.ID')
            ->leftJoin('BLOQUE_HORARIO', 'AUSENCIAS.ID', '=', 'BLOQUE_HORARIO.AUSENCIAS_ID', 'left outer')               
            ->where('AUSENCIAS.CONTRATO_ID', $request->CONTRATO_ID)
            ->where('AUSENCIAS.ACTIVO', 'S')
            ->get()
            ->toArray();


        $cuenta_corriente = DB::table('HORAS_GENERADAS')
            ->select(
                'CUENTAS_CTES.ID as cuenta_corriente_id',
                'DATOS_PACIENTE.RUT as rut_completo',
                'DATOS_PACIENTE.DV',
                'DATOS_PACIENTE.NOMBRES as nombres',
                'DATOS_PACIENTE.APELLIDO_PAT as apellido_pat',
                'DATOS_PACIENTE.APELLIDO_MAT as apellido_mat',
                'HORAS_GENERADAS.FECHA as fecha',
                'BLOQUE_HORARIO.CONTRATO_ID'


            )
            ->join('BLOQUE_HORARIO', 'HORAS_GENERADAS.BLOQUE_HORARIO_ID', '=', 'BLOQUE_HORARIO.ID')
            ->join('CUENTAS_CTES'  , 'CUENTAS_CTES.HORAS_GENERADAS_ID'  , '=', 'HORAS_GENERADAS.ID')
            ->join('DATOS_LOCALES' , 'CUENTAS_CTES.DATOS_LOCALES_ID'    , '=', 'DATOS_LOCALES.ID')
            ->join('DATOS_PACIENTE', 'DATOS_LOCALES.DATOS_PACIENTE_ID'  , '=', 'DATOS_PACIENTE.ID')
            ->where('BLOQUE_HORARIO.CONTRATO_ID', $request->CONTRATO_ID)
            ->whereBetween('HORAS_GENERADAS.FECHA', [ date("Y-m-d H:i:s", strtotime($fecha_desde)), date("Y-m-d H:i:s", strtotime($fecha_hasta))])
            ->where('CUENTAS_CTES.ACTIVO', 'S')
            ->orderBy('HORAS_GENERADAS.ID')
            ->get()
            ->toArray();

        $i = 0;
        $json = array();
        $json["respuesta"] = "0";

        if ($bloqueos) {
            foreach ($bloqueos as $bloqueo) {
                $bloqueo = (array) $bloqueo;
                if( $bloqueo['bloque_horario_id'] == '' && $bloqueo['motivo_ausencia_id'] == '29')
                {
                    // Si bloque horario está nulo y el motivo de la ausencia es por bloque horario por fecha,
                    // entonces no lo considere como ausencia porque el bloque horario ya fue borrado.

                } 
                else 
                {
                    $json["respuesta"] = "1";
                    $json["bloqueo"][$i]["id"] = $bloqueo["id"];
                    $json["bloqueo"][$i]["fecha_desde"] = $bloqueo["fecha_desde"];
                    $json["bloqueo"][$i]["fecha_hasta"] = $bloqueo["fecha_hasta"];
                    $i++;
                }
            }
            dd($json);
            while (is_array($bloqueos)) {
                
                $bloqueos = $miconexion->crea_arreglo($cons_bloqueos);
            }
        } 
        else {
            $json["respuesta"] = "0";
        }





        $j = 0;
        if(is_array($row_cta_ctes)){
            while(is_array($row_cta_ctes)){
                $row_cta_ctes = (array) $row_cta_ctes;
                $json["cuentas_ctes"][$j]["id_cuenta"] = $row_cta_ctes["cuenta_corriente_id"];
                $json["cuentas_ctes"][$j]["rut"] = $row_cta_ctes["rut_completo"];
                $json["cuentas_ctes"][$j]["nombres"] = $row_cta_ctes["nombres"];
                $json["cuentas_ctes"][$j]["ap_pat"] = $row_cta_ctes["apellido_pat"];
                $json["cuentas_ctes"][$j]["ap_mat"] = $row_cta_ctes["apellido_mat"];
                $json["cuentas_ctes"][$j]["fecha"] = $row_cta_ctes["fecha"];
                $j++;
                $row_cta_ctes = $miconexion->crea_arreglo($cons_cta_ctes);
            }
        }



        //dd($json);
        return $json;


    }    
}
