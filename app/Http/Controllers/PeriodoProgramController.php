<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use PHPUnit\Util\Exception;
use http\Message;
use Carbon\Carbon;

use App\Entities\PeriodoProgram;
use App\Http\Requests\Periodo_ProgramStoreRequest;
use App\Http\Requests\Listar_ProgramacionStoreRequest;

class PeriodoProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Listar_ProgramacionStoreRequest $object_request)
    {
        try
        {      
            $array_programacion = PeriodoProgram::where('ESTABLECIMIENTO_ID', '=', $object_request->ESTABLECIMIENTO_ID)
                                ->orderBy('ID', 'DESC')
                                ->get();
            for($i = 0; $i < count($array_programacion); $i++){
                $object_response['data'][$i]['type'] = "Periodo_Program";
                $object_response['data'][$i]['id'] = $array_programacion[$i]->id;
                $object_response['data'][$i]['attributes']['establecimiento_id'] = $array_programacion[$i]->establecimiento_id;
                $object_response['data'][$i]['attributes']['fecha_desde'] = $array_programacion[$i]->fecha_desde;
                $object_response['data'][$i]['attributes']['fecha_hasta'] = $array_programacion[$i]->fecha_hasta;
                $object_response['data'][$i]['attributes']['activo'] = $array_programacion[$i]->activo;
                $object_response['data'][$i]['attributes']['usuario_id_mod'] = $array_programacion[$i]->usuario_id_mod;
                $object_response['data'][$i]['attributes']['fecha_mod'] = $array_programacion[$i]->fecha_mod;
            }
            throw new HttpResponseException(response()->json($object_response, 200));   
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
                                    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Periodo_ProgramStoreRequest $object_request)
    {
        try
        {
            PeriodoProgram::where('ESTABLECIMIENTO_ID','=', $object_request->ESTABLECIMIENTO_ID)
                            ->where('ACTIVO','=','S')
                            ->update(['ACTIVO' => 'N', 
                                        'USUARIO_ID_MOD' => $object_request->USUARIO_ID_MOD, 
                                        'FECHA_MOD' => now()
                            ]);
            if (@$object_request->ACTIVO){
                $activo = $object_request->ACTIVO;
            }else{
                $activo = "S";
            }

            $array_periodo_program = PeriodoProgram::create([ 
                    'ESTABLECIMIENTO_ID' => $object_request->ESTABLECIMIENTO_ID, 
                    'FECHA_DESDE' =>  date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)),
                    'FECHA_HASTA' => date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA)),
                    'ACTIVO' => $activo, 
                    'USUARIO_ID_MOD' => $object_request->USUARIO_ID_MOD, 
                    'FECHA_MOD' => now()
            ]);       
            

            $object_response["success"] = true;
            $object_response["message"] = "Periodo de Programacion ingresado con exito";
            $object_response["data"][0]['id'] = $array_periodo_program->id;
            $object_response["data"][0]['id_establecimiento'] = $array_periodo_program->ESTABLECIMIENTO_ID;
            $object_response["data"][0]['fecha_desde'] = $array_periodo_program->FECHA_DESDE;
            $object_response["data"][0]['fecha_hasta'] = $array_periodo_program->FECHA_HASTA;
            $object_response["data"][0]['activo'] = $activo;
            $object_response["data"][0]['usuario_crea'] = $array_periodo_program->USUARIO_ID_MOD;
            $object_response["data"][0]['fecha_hasta'] = $array_periodo_program->FECHA_HASTA;
            throw new HttpResponseException(response()->json($object_response, 200));
            
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }
}
