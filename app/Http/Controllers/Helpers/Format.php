<?php

namespace App\Http\Controllers\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Format extends Controller
{
	public static function failedObjectResponse($field_name, $source, $type, $reference = NULL, $rango = NULL) {
		$array_details = array(
            'Alpha' => "El campo FAILED_FIELD_NAME debe contener solo caracteres alfabeticos",
    		'Required' => "Debe ingresar un valor para FAILED_FIELD_NAME",
    		'Integer' => "El valor de FAILED_FIELD_NAME debe ser un numero entero",
            'Max' => "El largo de FAILED_FIELD_NAME supera el maximo permitido",
            'Min' => "El largo de FAILED_FIELD_NAME es menor al minimo permitido",
    		'Date' => "El valor de FAILED_FIELD_NAME debe ser un fecha valida (Y-m-d H:i:s)",
			'In' => "El valor de FAILED_FIELD_NAME debe ser 'REFERENCE_FIELD_NAME' o 'RANGO_FIELD'",
			'Ipv4' => "El valor ingresado en FAILED_FIELD_NAME no corresponde a una direccion IPv4 valida",
            'Exists' => "El campo FAILED_FIELD_NAME no existe en la tabla REFERENCE_FIELD_NAME",
            'After' => "FAILED_FIELD_NAME no puede ser menor a REFERENCE_FIELD_NAME",
            'DateFormat' => "FAILED_FIELD_NAME no cumple con el formato PHP REFERENCE_FIELD_NAME",
            'Between' => 'El campo FAILED_FIELD_NAME debe tener un valor entre REFERENCE_FIELD_NAME y RANGO_FIELD',
            'Illuminate\Validation\ClosureValidationRule' => 'Error en el campo FAILED_FIELD_NAME'

        );
        $detail = str_replace("FAILED_FIELD_NAME", $field_name, $array_details[$type]);
        if (@$reference){
            $detail = str_replace("REFERENCE_FIELD_NAME", $reference, $detail);
        }
        if (@$rango){
            $detail = str_replace("RANGO_FIELD", $rango, $detail);
        }
    	$object_response = array(
    		'status' => 400, 
	        'source' => $source,
	        'title' => $field_name . " invalido",
	        'detail' => $detail
    	);    	
        return $object_response;
    }

    public static function nombreDias($numero_dia){
        $array_dias = [0 => 'Por Fecha', 1 => 'Lunes', 2 =>'Martes', 3 =>'Miercoles', 4 =>'Jueves', 5 =>'Viernes', 6 =>'Sabado'];
        return $array_dias[$numero_dia];
    }
}
