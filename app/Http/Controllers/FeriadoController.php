<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Entities\Feriado;
use App\Entities\HorasGeneradas;
use App\Entities\InterconsultaHoras;
use App\Entities\InterconsultaMovs;
use App\Entities\MovEstadoIc;
use App\Entities\CuentasCtes;
use App\Entities\HorasHistoricos;

use App\Http\Requests\Listar_FeriadosStoreRequest;
use App\Http\Requests\Crear_FeriadoStoreRequest;
use App\Http\Requests\Eliminar_FeriadoStoreRequest;
class FeriadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Listar_FeriadosStoreRequest $request)
    {
        try
        {       
            $array_feriados = Feriado::where('ESTABLECIMIENTO_ID', '=', $request->ESTABLECIMIENTO_ID)
                                     ->orderBy('ID', 'DESC')->get();
            for($i = 0; $i < count($array_feriados); $i++){
                $object_response['data'][$i]['type'] = "Feriado";
                $object_response['data'][$i]['id'] = $array_feriados[$i]->id;
                $object_response['data'][$i]['attributes']['establecimiento_id'] = $array_feriados[$i]->establecimiento_id;
                $object_response['data'][$i]['attributes']['fecha_desde'] = $array_feriados[$i]->fecha_desde;
                $object_response['data'][$i]['attributes']['fecha_hasta'] = $array_feriados[$i]->fecha_hasta;
                $object_response['data'][$i]['attributes']['activo'] = $array_feriados[$i]->activo;
                $object_response['data'][$i]['attributes']['usuario_id_mod'] = $array_feriados[$i]->usuario_id_mod;
                $object_response['data'][$i]['attributes']['fecha_mod'] = $array_feriados[$i]->fecha_mod;
                $object_response['data'][$i]['attributes']['ip_mod'] = $array_feriados[$i]->ip_mod;
                $object_response['data'][$i]['attributes']['usuario_crea_id'] = $array_feriados[$i]->usuario_crea_id;
                $object_response['data'][$i]['attributes']['fecha_crea'] = $array_feriados[$i]->fecha_crea;
                $object_response['data'][$i]['attributes']['ip_crea'] = $array_feriados[$i]->ip_crea;
            }
            throw new HttpResponseException(response()->json($object_response, 200));
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Crear_FeriadoStoreRequest $object_request)
    {
            try
            {
                $feriado = Feriado::where('ACTIVO','=', 'S')
                    ->where('ESTABLECIMIENTO_ID','=',$object_request->ESTABLECIMIENTO_ID)
                    ->whereBetween('FECHA_DESDE', [ date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)), date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA))])
                    ->whereBetween('FECHA_HASTA', [ date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)), date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA))])
                    ->get();
    
                if (count($feriado) == 0){

                    if (@$object_request->ACTIVO){
                        $activo = $object_request->ACTIVO;
                    }else{
                        $activo = "S";
                    }

                    $resp_fer = Feriado::create([ 
                        'ESTABLECIMIENTO_ID' => $object_request->ESTABLECIMIENTO_ID, 
                        'FECHA_DESDE' =>  date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)),
                        'FECHA_HASTA' => date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA)),
                        'ACTIVO' => $activo, 
                        'USUARIO_MOD_ID' => $object_request->USUARIO_CREA_ID,
                        'FECHA_MOD' => now(),
                        'IP_MOD' => $object_request->IP_CREA,
                        'USUARIO_CREA_ID' => $object_request->USUARIO_CREA_ID,
                        'FECHA_CREA' => now(),
                        'IP_CREA' => $object_request->IP_CREA                        
                    ]);             
                    
                    $fecha_desde = date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)); 
                    $fecha_hasta = date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA));

                    $results = DB::select( DB::raw(
                        "SELECT
                                hg.ID
                                ,cc.ID cuenta_id
                                ,ih.INTERCONSULTA_ID
                                ,oa.ID oa_id
                                ,hg.ESTADO_HORAS_ID
                                ,to_char(hg.FECHA,'DD/MM/YYYY HH24:MI:SS') fecha
                                ,bh.ID bloque_id
                                ,hg.TIPO_ATENCION_E_ID
                                ,hg.TIPO_ATENCION_P_ID
                                ,hg.TIPO_SOLICITUD_ID
                                ,to_char(hg.FECHA_ASIGNADA,'DD/MM/YYYY HH24:MI:SS') fecha_asignada
                                ,hg.USUARIO_ID_MOD
                                ,hg.INTERCONSULTA_ID
                                ,hg.OA_ID
                        FROM agenda.HORAS_GENERADAS hg
                            LEFT OUTER JOIN refcentral.CUENTAS_CTES cc ON hg.ID = cc.HORAS_GENERADAS_ID
                            INNER JOIN agenda.BLOQUE_HORARIO bh ON hg.BLOQUE_HORARIO_ID = bh.ID
                            INNER JOIN refcentral.CONTRATO contrato ON bh.CONTRATO_ID = contrato.ID
                            LEFT OUTER JOIN dbges.INTERCONSULTA_HORAS ih ON hg.ID = ih.HORAS_ASIGNADAS_ID
                            LEFT OUTER JOIN interconsulta.INTERCONSULTA oa ON hg.ID = oa.HORAS_ASIGNADAS_ID
                        WHERE hg.FECHA BETWEEN :fecha_desde AND :fecha_hasta
                            AND hg.ESTADO_HORAS_ID IN (1,4,5,7)
                            AND EXISTS (SELECT
                                            bh.ID
                                        FROM
                                            agenda.BLOQUE_HORARIO bh
                                        INNER JOIN refcentral.CONTRATO contrato ON bh.CONTRATO_ID = contrato.ID
                                        WHERE
                                            contrato.ACTIVO = 'S'
                                            AND contrato.ESTABLECIMIENTO_ID = :establecimiento_id
                                            AND hg.BLOQUE_HORARIO_ID = bh.ID)"), 
                    array(
                        'fecha_desde' => $fecha_hasta,
                        'fecha_hasta'=> $fecha_hasta,
                        'establecimiento_id' => $object_request->ESTABLECIMIENTO_ID
                    ));

                    foreach ($results as $clave => $valor){
                        if ($valor->interconsulta_id){
                            InterconsultaHoras::where('INTERCONSULTA_ID', '=', $valor->interconsulta_id)
                                                ->where('ACTIVO', '=', 'S')
                                                ->update(['ACTIVO' => 'N',
                                                        'USUARIO_ID_MOD' => $object_request->USUARIO_CREA_ID,
                                                        'FECHA_MOD' => now(),
                                                        'IP_MOD' => $object_request->IP_CREA,
                                                        'ESTADO_HORA_IC_ID' => 8]);

                            Interconsulta::where('ID', '=', $valor->interconsulta_id)
                                            ->update(['ESTADO_IC_ID' => 5,
                                                    'FECHA_MOD' => now(),
                                                    'USUARIO_ID_MOD' => $object_request->USUARIO_CREA_ID]);
                            
                            $interconsulta = Interconsulta::where('ID', '=', $valor->interconsulta_id)->get();
                            
                            InterconsultaMovs::create([
                                'INTERCONSULTA_ID' => $interconsulta[0]->interconsulta_id,
                                'FECHA' => now(),
                                'ESTAB_DESTINO_ID' => $interconsulta[0]->estab_destino_id,
                                'JERARQUIA_DEST_ID' => $interconsulta[0]->jerarquia_dest_id,
                                'TIPO_DERIVACION_ID' => $interconsulta[0]->tipo_derivacion_id,
                                'ESTADO_IC_ID' => 8,
                                'ESTADO_AUGE_ID' => $interconsulta[0]->estado_auge_id,
                                'ALERTA_ID' => $interconsulta[0]->alerta_id,
                                'DIAG_CIEL0_1_ID' => $interconsulta[0]->diag_cie10_1_id,
                                'DIAG_CIEL0_2_ID' => $interconsulta[0]->diag_cie10_2_id,
                                'RGS_ID' => $interconsulta[0]->rgs_id,
                                'ESTAB_ORIGEN_ID' => $interconsulta[0]->estab_origen_id,
                                'MOTIVO_INTERCONSULTA_ID' => $interconsulta[0]->motivo_interconsulta_id,
                                'USUARIO_ID_MOD' => $object_request->USUARIO_CREA_ID,
                                'OBSERVACION' => 'MODIFICADA POR EL USUARIO',
                                'ESTADO_IFL_ID' => $interconsulta[0]->estado_ifl_id,
                                'PROCEDENCIA_ID' => $interconsulta[0]->procedencia_id,
                                'N_DOCUMENTO_PROCEDENCIA' => $interconsulta[0]->n_documento_procedencia
                            ]);

                            InterconsultaMovs::create([
                                'INTERCONSULTA_ID' => $interconsulta[0]->interconsulta_id,
                                'FECHA' => now(),
                                'ESTAB_DESTINO_ID' => $interconsulta[0]->estab_destino_id,
                                'JERARQUIA_DEST_ID' => $interconsulta[0]->jerarquia_dest_id,
                                'TIPO_DERIVACION_ID' => $interconsulta[0]->tipo_derivacion_id,
                                'ESTADO_IC_ID' => 5,
                                'ESTADO_AUGE_ID' => $interconsulta[0]->estado_auge_id,
                                'ALERTA_ID' => $interconsulta[0]->alerta_id,
                                'DIAG_CIEL0_1_ID' => $interconsulta[0]->diag_cie10_1_id,
                                'DIAG_CIEL0_2_ID' => $interconsulta[0]->diag_cie10_2_id,
                                'RGS_ID' => $interconsulta[0]->rgs_id,
                                'ESTAB_ORIGEN_ID' => $interconsulta[0]->estab_origen_id,
                                'MOTIVO_INTERCONSULTA_ID' => $interconsulta[0]->motivo_interconsulta_id,
                                'USUARIO_ID_MOD' => $object_request->USUARIO_CREA_ID,
                                'OBSERVACION' => 'VUELVE A POR AGENDAR POR FERIADO',
                                'ESTADO_IFL_ID' => $interconsulta[0]->estado_ifl_id,
                                'PROCEDENCIA_ID' => $interconsulta[0]->procedencia_id,
                                'N_DOCUMENTO_PROCEDENCIA' => $interconsulta[0]->n_documento_procedencia
                            ]);

                        }   
                        if ($valor->oa_id){
                        
                            $interconsulta = Interconsulta::where('ID', '=', $valor->oa_id)->get();
                        
                            MovEstadoIc::create([
                                'ESTADO_IC_ID' => '10',
                                'FECHA_ESTADO' => now(),
                                'USUARIO_IC_ID' => $object_request->USUARIO_CREA_ID,
                                'ESTABLECIMIENTO_ID' => $interconsulta[0]->estab_origen_id,
                                'OBSERVACION' => 'Proceso de asignación de hora por sobrecupo',
                                'INTERCONSULTA_ID' => $valor->oa_id
                            ]);

                            Interconsulta::where('ID', '=', $valor->oa_id)
                                            ->update([
                                                'ESTADO_IC_ID' => 10,
                                                'USUARIO_ID_MOD' => $object_request->USUARIO_CREA_ID,
                                                'FECHA_MOD' => now(),
                                                'HORAS_ASIGNADAS_ID' => $interconsulta[0]->id
                                            ]);

                            CuentasCtes::where('ID', '=', $valor->cuenta_id)
                                        ->update([
                                            'OA_ID' => $valor->oa_id
                                        ]);
                        }             
                        if ($valor->cuenta_id){
                            
                            $cuenta_cte = CuentasCtes::where('ID', '=', $valor->cuenta_id)->get();

                            $resp_hh = HorasHistoricos::create([
                                'HORAS_GENERADAS_ID' => $cuenta_cte[0]->horas_generadas_id,
                                'BLOQUE_HORARIO_ID' => $valor->bloque_id,
                                'FECHA' => date("Y/m/d h:s", strtotime($valor->fecha)),
                                'ESTADO_HORAS_ID' => 6,
                                'TIPO_ATENCION_E_ID' => $valor->tipo_atencion_e_id,
                                'TIPO_ATENCION_P_ID' => $valor->tipo_atencion_p_id,
                                'TIPO_SOLICITUD_ID' => $valor->tipo_solicitud_id,
                                'FECHA_CREACION' => now(),
                                'FECHA_ASIGNADA' => date("Y/m/d h:s", strtotime($valor->fecha_asignada)),
                                'USUARIO_MOD_ID' => $object_request->USUARIO_CREA_ID,
                                'USR_DIO_HORA_ID' => $valor->usuario_id_mod,
                                'INTERCONSULTA_ID' => $valor->interconsulta_id,
                                'OA_ID' => $valor->oa_id
                            ]);

                            $id_hora_historico = $resp_hh->id;
                            CuentasCtes::where('ID', '=', $cuenta_cte[0]->id)
                                        ->update([
                                            'HORAS_GENERADAS_ID' => $id_hora_historico,
                                            'ESTADO_CTACTE_ID' => '10',
                                            'ACTIVO' => 'N',
                                            'USUARIO_ID_MOD' => $object_request->USUARIO_CREA_ID,
                                            'FECHA_MOD' => now(),
                                            'MOTIVO_ELIMINACION' => 'Se ha generado un Feriado id = '.$resp_fer->id
                                        ]);
                        }
                    } 

                    $hrs_gen = DB::update("UPDATE agenda.horas_generadas 
                                                SET estado_horas_id = 6
                                                WHERE fecha BETWEEN ? AND ?
                                                    AND estado_horas_id <> 9 
                                                    AND bloque_horario_id IN (SELECT a.id
                                                FROM agenda.bloque_horario a,
                                                    refcentral.contrato b
                                                WHERE a.contrato_id = b.id 
                                                    AND b.activo = 'S' 
                                                    AND b.establecimiento_id = ?)", [$fecha_desde, $fecha_hasta , $object_request->ESTABLECIMIENTO_ID]);


                    $object_response["success"] = true;
                    $object_response["message"] = "Feriado ingresado con exito";
                    $object_response["data"][0]['id'] = $resp_fer->id;
                    $object_response["data"][0]['id_establecimiento'] = $object_request->ESTABLECIMIENTO_ID;
                    $object_response["data"][0]['fecha_desde'] = $object_request->FECHA_DESDE;
                    $object_response["data"][0]['fecha_hasta'] = $object_request->FECHA_HASTA;
                    $object_response["data"][0]['activo'] = $activo;
                    $object_response["data"][0]['usuario_crea'] = $object_request->USUARIO_CREA_ID;
                    $object_response["data"][0]['ip_crea'] = $object_request->IP_CREA;
                    


                    throw new HttpResponseException(response()->json($object_response, 200));

                }else{
                    $object_response['errors'][0]['status'] = 402;
                    $object_response['errors'][0]['source'] = 'FeriadoController@store';
                    $object_response['errors'][0]['title'] = 'Fecha de feriado';
                    $object_response['errors'][0]['detail'] = "Feriado ingresado ya se encuentra registrado";

                    throw new HttpResponseException(response()->json($object_response, 402));
                }
        
            }
            catch (Exception $e)
            {
                $object_response["descripcion"] = $e;
                throw new HttpResponseException(response()->json($object_response, 402));
            }

    }


    /**
     * Update(Eliminar-Desactivar) the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Eliminar_FeriadoStoreRequest $request)
    {
          
        try
        {        
            $feriado = Feriado::where('ID','=',$request->ID_FERIADO)
                                ->where('ACTIVO','=', 'S')->get();
        
            if (count($feriado) > 0){
                $fecha_desde = $feriado[0]->fecha_desde;
                $fecha_hasta = $feriado[0]->fecha_hasta;
                $establecimiento_id = $feriado[0]->establecimiento_id;
                
                DB::update("UPDATE agenda.horas_generadas 
                        SET estado_horas_id = 5
                        WHERE estado_horas_id = 6 
                            AND fecha BETWEEN ? AND ?
                            AND ausencias_id = 0 
                            AND bloque_horario_id IN (SELECT a.id
                                                        FROM agenda.bloque_horario a,
                                                                refcentral.contrato b
                                                        WHERE a.contrato_id = b.id
                                                            AND b.activo = 'S'
                                                            AND b.establecimiento_id = ?)", [$fecha_desde, $fecha_hasta , $establecimiento_id]);

                DB::update("UPDATE agenda.horas_generadas hg 
                        SET hg.ESTADO_HORAS_ID = 4
                        WHERE hg.FECHA BETWEEN ? AND ?
                            AND hg.AUSENCIAS_ID > 0
                            AND EXISTS (SELECT id
                                            FROM refcentral.AUSENCIAS au
                                            WHERE au.ACTIVO = 'S'
                                                AND hg.AUSENCIAS_ID = au.ID)
                            AND EXISTS (SELECT bh.id
                                        FROM agenda.BLOQUE_HORARIO bh
                                            INNER JOIN refcentral.CONTRATO contrato ON bh.CONTRATO_ID = contrato.ID
                                        WHERE contrato.ACTIVO = 'S'
                                            AND contrato.ESTABLECIMIENTO_ID = ?
                                            AND hg.BLOQUE_HORARIO_ID = bh.ID)", [$fecha_desde, $fecha_hasta , $establecimiento_id]);
            
                Feriado::where('ID','=',$request->ID_FERIADO)
                    ->update(['ACTIVO' => 'N',
                            'FECHA_MOD' => now(),
                            'USUARIO_MOD_ID' => $request->ID_USUARIO]);
                $object_response["success"] = true;
                $object_response["message"] = "Feriado eliminado con exito";
                $object_response["data"][0]['id'] =$request->ID_FERIADO;
                $object_response["data"][0]['establecimiento_id'] =$establecimiento_id;
                $object_response["data"][0]['fecha_desde'] =$fecha_desde;
                $object_response["data"][0]['fecha_hasta'] =$fecha_hasta;
            
                throw new HttpResponseException(response()->json($object_response, 200));

            }else{
                $object_response["success"] = false;
                $object_response["message"] = "Feriado a eliminar no se encuentra Activo";
                $object_response["data"][0]['id'] =$request->ID_FERIADO;
            
                throw new HttpResponseException(response()->json($object_response, 402));
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }

    }

    /**
     * Metodo de verificacion para nuevos Feriados
     * chequea si existen horas agendadas para los mismos
     */
     public function verificaFeriado(Request $object_request)
     {   
        try
        {
            $feriado = Feriado::where('ACTIVO','=', 'S')
                ->where('ESTABLECIMIENTO_ID','=',$object_request->ESTABLECIMIENTO_ID)
                ->whereBetween('FECHA_DESDE', [ date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)), date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA))])
                ->whereBetween('FECHA_HASTA', [ date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)), date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA))])
                ->get();

            if (count($feriado) == 0){
                
                    $fecha_desde = date("Y/m/d h:s", strtotime($object_request->FECHA_DESDE)); 
                    $fecha_hasta = date("Y/m/d h:s", strtotime($object_request->FECHA_HASTA));
                    $establecimiento_id = $object_request->ESTABLECIMIENTO_ID;
                    $results = DB::select( DB::raw(
                        "SELECT
                                hg.ID
                                ,cc.ID cuenta_id
                                ,ih.INTERCONSULTA_ID
                                ,oa.ID oa_id
                                ,hg.ESTADO_HORAS_ID
                                ,to_char(hg.FECHA,'DD/MM/YYYY HH24:MI:SS') fecha
                                ,bh.ID bloque_id
                                ,hg.TIPO_ATENCION_E_ID
                                ,hg.TIPO_ATENCION_P_ID
                                ,hg.TIPO_SOLICITUD_ID
                                ,to_char(hg.FECHA_ASIGNADA,'DD/MM/YYYY HH24:MI:SS') fecha_asignada
                                ,hg.USUARIO_ID_MOD
                                ,hg.INTERCONSULTA_ID
                                ,hg.OA_ID
                        FROM agenda.HORAS_GENERADAS hg
                            LEFT OUTER JOIN refcentral.CUENTAS_CTES cc ON hg.ID = cc.HORAS_GENERADAS_ID
                            INNER JOIN agenda.BLOQUE_HORARIO bh ON hg.BLOQUE_HORARIO_ID = bh.ID
                            INNER JOIN refcentral.CONTRATO contrato ON bh.CONTRATO_ID = contrato.ID
                            LEFT OUTER JOIN dbges.INTERCONSULTA_HORAS ih ON hg.ID = ih.HORAS_ASIGNADAS_ID
                            LEFT OUTER JOIN interconsulta.INTERCONSULTA oa ON hg.ID = oa.HORAS_ASIGNADAS_ID
                        WHERE hg.FECHA BETWEEN :fecha_desde AND :fecha_hasta
                            AND hg.ESTADO_HORAS_ID IN (1,4,5,7)
                            AND EXISTS (SELECT
                                            bh.ID
                                        FROM
                                            agenda.BLOQUE_HORARIO bh
                                        INNER JOIN refcentral.CONTRATO contrato ON bh.CONTRATO_ID = contrato.ID
                                        WHERE
                                            contrato.ACTIVO = 'S'
                                            AND contrato.ESTABLECIMIENTO_ID = :establecimiento_id
                                            AND hg.BLOQUE_HORARIO_ID = bh.ID)"), 
                    array(
                        'fecha_desde' => $fecha_hasta,
                        'fecha_hasta'=> $fecha_hasta,
                        'establecimiento_id' => $establecimiento_id
                    )); 
                    
                    $total_horas = count($results);
                    if ($total_horas > 0){
                        $object_response["success"] = true;
                        $object_response["message"] = "La fecha ingresada tiene horas asociadas";
                        for($i = 0; $i < count($results); $i++){
                           $object_response["data"][$i]['id_hora_generada'] = $results[$i]->id;
                           $object_response["data"][$i]['fecha_desde'] =  $fecha_desde;
                           $object_response["data"][$i]['fecha_hasta'] =  $fecha_hasta;
                           $object_response["data"][$i]['establecimiento_id'] =  $establecimiento_id;
                        }
                        throw new HttpResponseException(response()->json($object_response, 200));
                    }else{
                        $object_response["success"] = true;
                        $object_response["message"] = "La fecha ingresada no tiene horas asociadas";                        
                        $object_response["data"][0]['fecha_desde'] =  $fecha_desde;
                        $object_response["data"][0]['fecha_hasta'] =  $fecha_hasta;
                        $object_response["data"][0]['establecimiento_id'] =  $establecimiento_id;
                        throw new HttpResponseException(response()->json($object_response, 200));
                    }
              
            }else{
                $object_response["success"] = false;
                $object_response["message"] = "La fecha ingresada existe como feriado";
                $object_response["data"][0]['id'] =$object_request->ID_FERIADO;
            
                throw new HttpResponseException(response()->json($object_response, 402));
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
     }

}
