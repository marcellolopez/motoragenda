<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 17/12/2019
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Carbon\Carbon;
use App\Http\Controllers\Helpers\Format;


use App\Entities\Persona;
use App\Entities\Contrato;
use App\Entities\BloqueHorario;
use App\Entities\Ausencia;
use App\Entities\HorasGeneradas;

use App\Http\Requests\BloquesHorariosProfesionalRequest;
use App\Http\Requests\ListadoBloqueosProfesionalRequest;
<<<<<<< HEAD
use App\Http\Requests\HorasReasignardeBloqueHorarioRequest;
=======
use App\Http\Requests\ObtenerProfesionalCantidadBloqueosRequest;

>>>>>>> 5d2b02b4d3775f1ef6b5b09e9299e9083bb2de8b


class HorasReasignarController extends Controller
{
    

    /**
     * OBTENER BLOQUES HORARIOS DEL PROFESIONAL
     */
    public function bloquesHorarioProfesional(BloquesHorariosProfesionalRequest $object_request)
    {
        $resp_profesional = Persona::where('RUT', $object_request->RUT)
                                   ->where('ACTIVO', 'S') ->first();
        if ($resp_profesional)
        {
            $resp_contratos = Contrato::where('persona_id', $resp_profesional->id)
                                      ->where('ACTIVO', 'S')->get();
            if ($resp_contratos)
            {
                $ind_contr = 0;
                $object_response['data'][$ind_contr]['type'] = "Bloque_Horario Activo de Profesional";
                $object_response['data'][$ind_contr]['rut_profesional'] = $object_request->RUT."-".$resp_profesional->dv;
                $object_response['data'][$ind_contr]['id_profesional'] = $resp_profesional->id;
                $object_response['data'][$ind_contr]['nombres'] = $resp_profesional->nombres;
                $object_response['data'][$ind_contr]['apellido_pat'] = $resp_profesional->apellido_pat;
                $object_response['data'][$ind_contr]['apellido_mat'] = $resp_profesional->apellido_mat;
                $object_response['data'][$ind_contr]['fono_oficina'] = $resp_profesional->fono_oficina;
                $object_response['data'][$ind_contr]['email'] = $resp_profesional->email;
              
                foreach($resp_contratos as $contrato){
                    $ind_bh = 0;                    
                    $resp_bh = BloqueHorario::where('CONTRATO_ID', $contrato->id)
                                            ->where('ACTIVO', 'S')->get();
                    $object_response['data'][$ind_contr]['attributes']['contrato_id'] = $contrato->id;                                            
                    if ($resp_bh)
                    {
                        foreach($resp_bh as $key => $value){
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['id'] = $value->id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['contrato_id'] = $value->contrato_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['jerarquia_espec_id'] = $value->jerarquia_espec_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['dia'] = $value->dia;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['tiempo_x_paciente'] = $value->tiempo_x_paciente;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['hora_inicio'] = $value->hora_inicio;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['hora_fin'] = $value->hora_fin;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['tipo_bloque_id'] = $value->tipo_bloque_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['activo'] = $value->activo;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['usuario_mod_id'] = $value->usuario_mod_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['fecha_mod'] = $value->fecha_mod;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bh]['ausencias_id'] = $value->ausencias_id;
                            $ind_bh++;
                        }
                    }else{
                        $object_response['data'][$ind_contr]['detail'] = 'Contrato sin bloques horarios'; 
                    }
                    $ind_contr++;
                }   
                throw new HttpResponseException(response()->json($object_response, 200));
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'HorasReasignarController@bloquesHorarioProfesional';
                $object_response['errors'][0]['title'] = 'Profesional sin contratos';
                $object_response['errors'][0]['detail'] = 'Profesional ingresado no tiene contratos activos.';
                throw new HttpResponseException(response()->json($object_response, 400)); 
            }

        }else{
            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'HorasReasignarController@bloquesHorarioProfesional';
            $object_response['errors'][0]['title'] = 'No se encontro Profesional';
            $object_response['errors'][0]['detail'] = 'Profesional ingresado no se encuentra activo.';
            throw new HttpResponseException(response()->json($object_response, 400));    
        }

    }
    
    /**
     * LISTAR BLOQUEOS DE UN PROFESIONAL
     */
    public function listadoBloqueosProfesional(ListadoBloqueosProfesionalRequest $object_request)
    {
        $resp_profesional = Persona::where('RUT', $object_request->RUT)
                                    ->where('ACTIVO', 'S') ->first();
        if ($resp_profesional)
        {
            $resp_contratos = Contrato::where('persona_id', $resp_profesional->id)
                    ->where('ACTIVO', 'S')->get();
            if ($resp_contratos)
            {
                $ind_contr = 0;

                $object_response['data'][$ind_contr]['type'] = "Ausencias de Profesional";
                $object_response['data'][$ind_contr]['rut_profesional'] = $object_request->RUT;
                $object_response['data'][$ind_contr]['id_profesional'] = $resp_profesional->id;
                $object_response['data'][$ind_contr]['nombres'] = $resp_profesional->nombres;
                $object_response['data'][$ind_contr]['apellido_pat'] = $resp_profesional->apellido_pat;
                $object_response['data'][$ind_contr]['apellido_mat'] = $resp_profesional->apellido_mat;
                $object_response['data'][$ind_contr]['fono_oficina'] = $resp_profesional->fono_oficina;
                $object_response['data'][$ind_contr]['email'] = $resp_profesional->email;
                foreach($resp_contratos as $contrato){

                    $resp_ausencia = Ausencia::where('CONTRATO_ID', $contrato->id)
                                             ->where('ACTIVO', 'S')->get();
                    if ($resp_ausencia)
                    {
                        
                        $ind_bq = 0;
                        foreach($resp_ausencia as $key => $value){
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['id'] = $value->id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['contrato_id'] = $value->contrato_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['fecha'] = $value->fecha;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['fecha_desde'] = $value->fecha_desde;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['fecha_hasta'] = $value->fecha_hasta;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['motivo_ausencia_id'] = $value->motivo_ausencia_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['cantiadad_pacientes'] = $value->cantiadad_pacientes;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['destino_pacient_id'] = $value->destino_pacient_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['solicitant_bloq_id'] = $value->solicitant_bloq_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['observacion'] = $value->observacion;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['activo'] = $value->activo;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['usuario_id_mod'] = $value->usuario_id_mod;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['fecha_mod'] = $value->fecha_mod;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['bloque_horario_id'] = $value->bloque_horario_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['usuario_crea_id'] = $value->usuario_crea_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['fecha_crea'] = $value->fecha_crea;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['usuario_mod_id'] = $value->usuario_mod_id;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['ip_crea'] = $value->ip_crea;
                            $object_response['data'][$ind_contr]['attributes'][$ind_bq]['ip_mod'] = $value->ip_mod;
                            $ind_bq++;
                            
                        }
                    }else{
                        $object_response['data'][$ind_contr]['detail'] = 'Contrato sin bloques horarios'; 
                    }
                    $ind_contr++;

                }
            
                throw new HttpResponseException(response()->json($object_response, 200));
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'HorasReasignarController@listadoBloqueosProfesional';
                $object_response['errors'][0]['title'] = 'Profesional sin contratos';
                $object_response['errors'][0]['detail'] = 'Profesional ingresado no tiene contratos activos.';
                throw new HttpResponseException(response()->json($object_response, 400)); 
            }

        }else{
            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'HorasReasignarController@listadoBloqueosProfesional';
            $object_response['errors'][0]['title'] = 'No se encontro Profesional';
            $object_response['errors'][0]['detail'] = 'Profesional ingresado no se encuentra activo.';
            throw new HttpResponseException(response()->json($object_response, 400));    
        }
    }

    /**
     * DATOS PROFESIONAL Y CANTIDAD DE BLOQUEOS
     */
    public function obtenerProfesionalCantidadBloqueos(ObtenerProfesionalCantidadBloqueosRequest $request)
    {

        $contrato  = Contrato::with('persona')
            ->where('CONTRATO.PERSONA_ID', $request->get('PERSONA_ID'))
            ->where('CONTRATO.ID', $request->get('CONTRATO_ID'))
            ->where('CONTRATO.ACTIVO', 'S')
            ->first();

        if(!$contrato)
        {
            $object_response["descripcion"] = "Contrato no se encuentra activo";

            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'HorasReasignarController@obtenerProfesionalCantidadBloqueos';
            $object_response['errors'][0]['title'] =  'Contrato inactivo';
            $object_response['errors'][0]['detail'] = "Contrato no se encuentra activo";                
            throw new HttpResponseException(response()->json($object_response, 402));
        }
        else
        {   
            $cantidad_bloqueos = Ausencia::where('contrato_id', $request->get('CONTRATO_ID'))
                ->where('USUARIO.ACTIVO', 'S')
                ->join('USUARIO', 'AUSENCIAS.USUARIO_ID_MOD', '=', 'USUARIO.ID')  
                ->count();
   
        $object_response['data']['persona']['type'] = "Persona";
        $object_response['data']['persona']['attributes']['id'] = $contrato->persona->id;
        $object_response['data']['persona']['attributes']['nombres'] = $contrato->persona->nombres;
        $object_response['data']['persona']['attributes']['apellido_pat'] = $contrato->persona->apellido_pat;
        $object_response['data']['persona']['attributes']['apellido_mat'] = $contrato->persona->apellido_mat;

        $object_response['data']['contrato']['type'] = "Contrato";
        $object_response['data']['contrato']['attributes']['id'] = $contrato->id;  
        $object_response['data']['contrato']['attributes']['Establecimiento_id'] = $contrato->establecimiento_id;   
        $object_response['data']['bloqueos']['type'] = "Bloqueo"; 
        $object_response['data']['bloqueos']['attributes']['Cantidad de bloqueos'] = $cantidad_bloqueos; 

        }
        throw new HttpResponseException(response()->json($object_response, 200));
    }

    /**
     * HORAS A REASIGNAR POR BLOQUE HORARIO
     */
    public function horasReasignardeBloqueHorario(HorasReasignardeBloqueHorarioRequest $request){

        $id_bh = $request->BLOQUE_HORARIO_ID;
        $resp_horas = HorasGeneradas::where('HORAS_GENERADAS.BLOQUE_HORARIO_ID', $id_bh)
                                    ->join('AUSENCIAS', 'AUSENCIAS.BLOQUE_HORARIO_ID', '=', 'HORAS_GENERADAS.BLOQUE_HORARIO_ID')
                                    ->join('REFCENTRAL.CUENTAS_CTES', 'CUENTAS_CTES.HORAS_GENERADAS_ID', '=', 'HORAS_GENERADAS.ID')
                                    ->join('REFCENTRAL.DATOS_LOCALES', 'DATOS_LOCALES.ID', '=', 'CUENTAS_CTES.DATOS_LOCALES_ID')
                                    ->join('REFCENTRAL.DATOS_PACIENTE', 'DATOS_PACIENTE.ID', '=', 'DATOS_LOCALES.DATOS_PACIENTE_ID')
                                    ->select('HORAS_GENERADAS.ID',
                                             'HORAS_GENERADAS.FECHA',
                                             'HORAS_GENERADAS.ESTADO_HORAS_ID',
                                             'HORAS_GENERADAS.TIPO_ATENCION_E_ID',
                                             'HORAS_GENERADAS.TIPO_ATENCION_P_ID',
                                             'DATOS_PACIENTE.NOMBRES',
                                             'DATOS_PACIENTE.APELLIDO_PAT',
                                             'DATOS_PACIENTE.APELLIDO_MAT',
                                             'DATOS_PACIENTE.DIRECCION',
                                             'DATOS_PACIENTE.COMUNA_ID',
                                             'DATOS_LOCALES.FONO_CASA',
                                             'DATOS_LOCALES.FONO_MOVIL',
                                             'DATOS_LOCALES.NOMBRE_CONTACTO',
                                             'DATOS_LOCALES.FONO_CONTACTO')
                                    ->orderBy('HORAS_GENERADAS.ID')
                                    ->get();
        
        if ($resp_horas){                                    
            $object_response['data']['type'] = "Bloque Horario";
            $object_response['data']['attributes']['id'] = $id_bh;
            
            $ind_hg = 0;
            foreach ($resp_horas as $value){

                $object_response['data'][$ind_hg]['datos_paciente']['type'] = "Datos Paciente";
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['id'] = $value->id;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['nombres'] = $value->nombres;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['apellido_pat'] = $value->apellido_pat;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['apellido_mat'] = $value->apellido_mat;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['direccion'] = $value->direccion;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['comuna_id'] = $value->comuna_id;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['fono_casa'] = $value->fono_casa;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['fono_movil'] = $value->fono_movil;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['nombre_contacto'] = $value->nombre_contacto;
                $object_response['data'][$ind_hg]['datos_paciente']['attributes']['fono_contacto'] = $value->fono_contacto;

                $object_response['data'][$ind_hg]['hora_generada']['type'] = "Hora Generada";
                $object_response['data'][$ind_hg]['hora_generada']['attributes']['id'] = $value->id;
                $object_response['data'][$ind_hg]['hora_generada']['attributes']['fecha'] = $value->fecha;
                $object_response['data'][$ind_hg]['hora_generada']['attributes']['estado_horas_id'] = $value->estado_horas_id;
                $object_response['data'][$ind_hg]['hora_generada']['attributes']['tipo_atencion_e_id'] = $value->tipo_atencion_e_id;
                $object_response['data'][$ind_hg]['hora_generada']['attributes']['tipo_atencion_p_id'] = $value->tipo_atencion_p_id;
                
                $ind_hg++;
            }
            throw new HttpResponseException(response()->json($object_response, 200));
        }else{
            $object_response["descripcion"] = "Bloque horario no posee horas a reasignar";

            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'HorasReasignarController@horasReasignardeBloqueHorario';
            $object_response['errors'][0]['title'] =  'Contrato inactivo';
            $object_response['errors'][0]['detail'] = "Contrato no se encuentra activo";                
            throw new HttpResponseException(response()->json($object_response, 402));           
        }

    }



}



