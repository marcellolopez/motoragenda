<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 17/12/2019
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Carbon\Carbon;
use App\Http\Controllers\Helpers\Format;


use App\Entities\Persona;
use App\Entities\Contrato;
use App\Entities\BloqueHorario;
use App\Entities\JerarquiaEspec; 
use App\Entities\AtencionEstab; 
use App\Entities\DatosPaciente; 
use App\Entities\HorasGeneradas; 
use App\Entities\CuentasCtes; 
use App\Entities\Interconsulta; 
use App\Entities\InterconsultaMovs; 
use App\Entities\InterconsultaHoras; 
use App\Entities\MovEstadoIc; 

use App\Http\Requests\BuscaProfesionalRequest;
use App\Http\Requests\buscarBloquesHorariosRequest;
use App\Http\Requests\CrearSobrecupoRequest;
use App\Http\Requests\ListarSobrecuposPacientesRequest;
;

class SobreCupoController extends Controller
{
    /**
     * 
     * INFORMACION DE PROFECIONAL POR RUT(Y ESTABLECIMIENTO) Y ESTABLECIMIENTOS DEL MISMO
     * 
     */
    public function index(BuscaProfesionalRequest $object_request)
    {
        try
        {
            $response_prof = Persona::where('RUT', $object_request->RUT)->first();

            //agregar info persona a json
            $object_response['data'][0]['type'] = "Profesional (Persona)";
            $object_response['data'][0]['id_persona'] = $response_prof->id;
            $object_response['data'][0]['attributes']['rut'] = $response_prof->rut;
            $object_response['data'][0]['attributes']['dv'] = $response_prof->dv;
            $object_response['data'][0]['attributes']['nombres'] = $response_prof->nombres;
            $object_response['data'][0]['attributes']['apellido_pat'] = $response_prof->apellido_pat;
            $object_response['data'][0]['attributes']['apellido_mat'] = $response_prof->apellido_mat;
            $object_response['data'][0]['attributes']['sexo_id'] = $response_prof->sexo_id;
            $object_response['data'][0]['attributes']['direccion'] = $response_prof->direccion;
            $object_response['data'][0]['attributes']['estado_civil_id'] = $response_prof->estado_civil_id;
            $object_response['data'][0]['attributes']['comunas_id'] = $response_prof->comunas_id;
            $object_response['data'][0]['attributes']['fono_casa'] = $response_prof->fono_casa;
            $object_response['data'][0]['attributes']['fono_oficina'] = $response_prof->fono_oficina;
            $object_response['data'][0]['attributes']['fono_movil'] = $response_prof->fono_movil;
            $object_response['data'][0]['attributes']['email'] = $response_prof->email;
            $object_response['data'][0]['attributes']['activo'] = $response_prof->activo;
            $object_response['data'][0]['attributes']['usuario_mod_id'] = $response_prof->usuario_mod_id;
            $object_response['data'][0]['attributes']['fecha_mod'] = $response_prof->fecha_mod;
            $object_response['data'][0]['attributes']['imei1'] = $response_prof->imei1;
            $object_response['data'][0]['attributes']['imei2'] = $response_prof->imei2;
            $object_response['data'][0]['attributes']['desde_ins'] = $response_prof->desde_ins;
            $object_response['data'][0]['attributes']['validado_api_minsal'] = $response_prof->validado_api_minsal;
            $object_response['data'][0]['attributes']['fecha_nacimiento'] = $response_prof->fecha_nacimiento;

            $response_contratos = Contrato::where('PERSONA_ID', $response_prof->id)
                                        ->where('ESTABLECIMIENTO_ID', $object_request->ESTABLECIMIENTO_ID)
                                        ->where('ACTIVO', 'S')
                                        ->orderBy('ID', 'ASC')->get();

            $ind_contrato = 1;
            if (count($response_contratos) > 0)
            {
                foreach($response_contratos as  $contrato)
                {
                    $response_bh = BloqueHorario::with('jerarquia_espec')->select('jerarquia_espec_id')
                                            ->where('contrato_id', $contrato->id)
                                            ->where('activo', 'S')
                                            ->groupBy('jerarquia_espec_id')
                                            ->get();
                $object_response['data'][$ind_contrato]['type'] = "Policlinicos del Profesional";
                $object_response['data'][$ind_contrato]['contrato_id'] = $contrato->id;
                $ind_poli = 0;
                foreach($response_bh as $policlinico)
                {
                    $object_response['data'][$ind_contrato][$ind_poli]['attributes']['jerarquia_espec_id'] = $policlinico->jerarquia_espec_id;
                    $object_response['data'][$ind_contrato][$ind_poli]['attributes']['policlinico_id'] = $policlinico->jerarquia_espec->policlinico->id;
                    $object_response['data'][$ind_contrato][$ind_poli]['attributes']['descripcion'] = $policlinico->jerarquia_espec->policlinico->descripcion;
                    $ind_poli++;
                }
                $ind_contrato++;
                }
                
            }else{
                $object_response['data'][$ind_contrato]['type'] = "Policlinicos del Profesional";
                $object_response['data'][$ind_contrato]['status'] = false;
                $object_response['data'][$ind_contrato]['message'] = "Profesional no posee contratos con este establecimiento";
            }
            throw new HttpResponseException(response()->json($object_response, 200));  
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }            
    }
    
    /**
     * BUSQUEDA DE BLOQUES HORARIO DISPONIBLES SEGUN POLICLINICO Y FECHA
     */
    public function buscarBloquesHorarios(buscarBloquesHorariosRequest $object_request)
    {
        
        try
        {      
            $fechaCarbon =  Carbon::parse($object_request->FECHA); 
            $dia = $fechaCarbon->isoFormat('d'); 
            
            $response_prof = Persona::where('RUT', $object_request->RUT)->first();

            $response_contratos = Contrato::where('PERSONA_ID', $response_prof->id)
                                        ->where('ESTABLECIMIENTO_ID', $object_request->ESTABLECIMIENTO_ID)
                                        ->where('ACTIVO', 'S')
                                        ->orderBy('ID', 'ASC')->get();

            if (count($response_contratos) > 0)
            {
                $object_response['data'][0]['type'] = "Bloques Horarios (Por Profesional/Policlinico/Fecha)";
                $object_response['data'][0]['id_persona'] = $response_prof->id;
                $object_response['data'][0]['jerarquia_espec_id'] = $object_request->JERARQUIA_ESPEC_ID;
                $object_response['data'][0]['fecha'] = $object_request->FECHA;
                
                foreach($response_contratos as  $contrato)
                {     

                    $response_bh = BloqueHorario::with('jerarquia_espec')
                                        ->where('jerarquia_espec_id', $object_request->JERARQUIA_ESPEC_ID)
                                        ->where('contrato_id', $contrato->id)
                                        ->where('activo', 'S')
                                        ->where('dia', $dia)
                                        ->orderBy('hora_inicio', 'ASC')
                                        ->get();
                    $ind_bloque = 0;
                    foreach($response_bh as $bloquehorario)
                    {
                        $object_response['data'][$ind_bloque]['attributes']['contrato_id'] = $contrato->id;
                        $object_response['data'][$ind_bloque]['attributes']['bloque_id'] = $bloquehorario->id;
                        $object_response['data'][$ind_bloque]['attributes']['jerarquia_espec_id'] = $bloquehorario->jerarquia_espec_id;
                        $object_response['data'][$ind_bloque]['attributes']['hora_inicio'] = $bloquehorario->hora_inicio;
                        $object_response['data'][$ind_bloque]['attributes']['hora_fin'] = $bloquehorario->hora_fin;
                        $object_response['data'][$ind_bloque]['attributes']['dia'] = Format::nombreDias($bloquehorario->dia);
                        $object_response['data'][$ind_bloque]['attributes']['policlinico_id'] = $bloquehorario->jerarquia_espec->policlinico->id;
                        $object_response['data'][$ind_bloque]['attributes']['policlinico_descripcion'] = $bloquehorario->jerarquia_espec->policlinico->descripcion;
                        $object_response['data'][$ind_bloque]['attributes']['tiempo_x_paciente'] = $bloquehorario->tiempo_x_paciente;
                        $object_response['data'][$ind_bloque]['attributes']['tipo_bloque_id'] = $bloquehorario->tipo_bloque_id;

                        $ind_bloque++;
                    }

                }
                throw new HttpResponseException(response()->json($object_response, 200));
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'SobreCupoController@buscarBloquesHorarios';
                $object_response['errors'][0]['title'] = 'No se encontraron contratos';
                $object_response['errors'][0]['detail'] = 'No existen contratos del profesional en el establecimiento ingresado';
                throw new HttpResponseException(response()->json($object_response, 400));
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }         


    }

    /**
     * REGISTRO DE SOBRECUPO
     */
    public function store(CrearSobrecupoRequest $object_request)
    {
        try
        {
            $rut_paciente = $object_request->RUT_PACIENTE;
            $bloque_id = $object_request->BLOQUE_HORARIO_ID;
            $tipo_atencion_id = $object_request->TIPO_ATENCION_ID;
            $fechaCarbon =  Carbon::parse($object_request->FECHA_SC); 
            $fecha_sc = date("Y/m/d H:i", strtotime($object_request->FECHA_SC." ".$object_request->HORA));
            $dia_sc = $fechaCarbon->isoFormat('d');
            $ic_id = $object_request->IC_ID;
            $oa_id = $object_request->OA_ID;
            $usuario = $object_request->USUARIO_CREA_ID;
            $ip_crea = $object_request->IP_CREA;

            /* VERIFICO BLOQUE HORARIO ACTIVO Y OBTENGO ESTABLECIMIENTO Y POLICLINICO */
            $resp_bh = BloqueHorario::with('contrato')
                                    ->where('ID', $bloque_id )
                                    ->where('ACTIVO','S')->first();

        // dd($resp_bh);
            if ($resp_bh)
            {
                $estab_id = $resp_bh->contrato->establecimiento_id;
                $jerarquia_espec_id = $resp_bh->jerarquia_espec_id;
                $dia_bh = $resp_bh->dia;
                if ($dia_sc != $dia_bh){
                    $object_response['errors'][0]['status'] = 400;
                    $object_response['errors'][0]['source'] = 'SobreCupoController@store';
                    $object_response['errors'][0]['title'] = 'No se creo sobrecupo';
                    $object_response['errors'][0]['detail'] = 'Fecha no se corresponde con dia de bloque horario ingresado';
                    throw new HttpResponseException(response()->json($object_response, 400));
                }else{

                    /* VERIFICO TIPO DE ATENCION->ESTABLECIMIENTO DE BLOQUE HORARIO */
                    $resp_tipo_act = AtencionEstab::with('tipo_atencion')
                                                ->where('TIPO_ATENCION_ID', $tipo_atencion_id)
                                                ->where('ESTABLECIMIENTO_ID', $estab_id)->first();
                    if ($resp_tipo_act)
                    {
                        $nombre_tipo_atencion = $resp_tipo_act->tipo_atencion->descripcion;

                        /* VERIFICO EXISTENCIA DE PACIENTE */
                        $resp_paciente = DatosPaciente::where('RUT', $rut_paciente)
                                                    ->where('ACTIVO', 'S')->first();
                        if ($resp_paciente)
                        {
                            /* OBTENGO NOMBRE POLICLINICO */
                            $resp_poli = JerarquiaEspec::with('policlinico')
                                                    ->where('ID', $jerarquia_espec_id)
                                                    ->first();
                            $policlinico = $resp_poli->policlinico->descripcion;
                            
                            /* SE CREA HORA */
                            $valido_hora = HorasGeneradas::where('BLOQUE_HORARIO_ID', $bloque_id)
                                                        ->where('FECHA', $fecha_sc)->first();
                            if ($valido_hora)
                            {
                                $object_response['errors'][0]['status'] = 400;
                                $object_response['errors'][0]['source'] = 'SobreCupoController@store';
                                $object_response['errors'][0]['title'] = 'No se creo sobrecupo';
                                $object_response['errors'][0]['detail'] = 'La hora ingresada ya se encuentra registrada';
                                throw new HttpResponseException(response()->json($object_response, 400));
                            }
                            else
                            {

                                $datos_paciente = DB::select( DB::raw(
                                    "SELECT dlocal.ID	  
                                        FROM refcentral.DATOS_PACIENTE dpac
                                        INNER JOIN refcentral.DATOS_LOCALES dlocal ON dpac.ID = dlocal.DATOS_PACIENTE_ID
                                        WHERE dpac.RUT = :rut
                                            AND dlocal.ESTABLECIMIENTO_ID = :establecimiento_id"), 
                                array(
                                    'rut' => $rut_paciente,
                                    'establecimiento_id' => $estab_id
                                ));
                                
                                if ($datos_paciente){

                                    $resp_hg = HorasGeneradas::create([
                                        'BLOQUE_HORARIO_ID' => $bloque_id,
                                        'FECHA' => $fecha_sc,
                                        'ESTADO_HORAS_ID' => 7,
                                        'TIPO_ATENCION_E_ID' => $tipo_atencion_id,
                                        'TIPO_ATENCION_P_ID' => $tipo_atencion_id,
                                        'TIPO_SOLICITUD_ID' => 7,
                                        'FECHA_CREACION' => now(),
                                        'FECHA_ASIGNADA' => now(),
                                        'USUARIO_ID_MOD' => $usuario,
                                        'INTERCONSULTA_ID' => $ic_id,
                                        'OA_ID' => $oa_id,
                                        'ES_SOBRECUPO' => 'S'
                                    ]);

                                    if($resp_hg){
                                        $id_hora_generada = $resp_hg->id;
                                        
                                        $resp_cts = CuentasCtes::create([
                                                'datos_locales_id' => $datos_paciente[0]->id,
                                                'horas_generadas_id' => $id_hora_generada,
                                                'documento_anterior' => 0,
                                                'tipo_documento_id' => 1,
                                                'activo' => 'S',
                                                'estado_ctacte_id' => 1,
                                                'fecha_creacion' => now(),
                                                'usuario_crea_id' => $usuario,
                                                'interconsulta_id' => $ic_id,
                                                'oa_id' => $oa_id,
                                                'ip_crea' => $ip_crea
                                        ]);
                                        
                                        if ($ic_id)
                                        {
                                            $resp_ic = Interconsulta::where('ID', $ic_id)->first();

                                            $resp_ic_mov = InterconsultaMovs::create([
                                                'interconsulta_id' => $ic_id,
                                                'fecha' => now(),
                                                'estab_destino_id' => $resp_ic->estab_destino_id,
                                                'jerarquia_dest_id' => $resp_ic->jerarquia_dest_id,
                                                'tipo_derivacion_id' => $resp_ic->tipo_derivacion_id,
                                                'estado_ic_id' => 8,
                                                'estado_auge_id' => $resp_ic->estado_auge_id,
                                                'alerta_id' => $resp_ic->alerta_id,
                                                'diag_cie10_1_id' => $resp_ic->diag_cie10_1_id,
                                                'diag_cie10_2_id' => $resp_ic->diag_cie10_2_id,
                                                'rgs_id' =>$resp_ic->rgs_id,
                                                'estab_origen_id' => $resp_ic->estab_origen_id,
                                                'motivo_interconsulta_id' => $resp_ic->motivo_interconsulta_id,
                                                'usuario_id_mod' => $usuario,
                                                'observacion' => 'MODIFICADA POR EL USUARIO',
                                                'estado_ifl_id' => $resp_ic->estado_ifl_id,
                                                'procedencia_id' => $resp_ic->procedencia_id,
                                                'n_documento_procedencia' => $resp_ic->n_documento_procedencia
                                            ]);

                                            $resp_ic_hrs = InterconsultaHoras::create([
                                                'interconsulta_id' => $ic_id,
                                                'horas_asignadas_id' => $id_hora_generada,
                                                'activo' => 'S',
                                                'usuario_id_mod' => $usuario,
                                                'fecha_mod' => now(),
                                                'estado_ifl_id' => 0,
                                                'usuario_crea_id' => $usuario,
                                                'fecha_crea' => now(),
                                                'ip_crea' => $ip_crea,
                                                'estado_hora_ic_id' => 1,
                                                'establecimiento_que_agenda_id' => $estab_id,
                                                'usuario_que_agenda_id' => $usuario,
                                                'fecha_agendamiento' => now(),
                                                'fecha_atencion' => $fecha_sc,
                                                'procedencia_hora' => 'AGEN_SOBRE_CUPO'
                                            ]);

                                            $resp_ic_upd = Interconsulta::where('id', $ic_id)
                                                            ->update([
                                                                'estado_ic_id' => 10,
                                                                'usuario_id_mod' => $usuario,
                                                                'fecha_mod' => now()
                                            ]);

                                            $resp_cts_upd = CuentasCtes::where('ID', $resp_cts->id)
                                                                ->update([
                                                                    'interconsulta_id' => $ic_id
                                            ]);
                                            
                                        }

                                        if ($oa_id)
                                        {
                                            $resp_ic = DB::select( DB::raw(
                                                "SELECT 	A.ESTAB_ORIGEN_ID, 
                                                            A.ESTADO_IC_ID, 
                                                            A.FECHA_MOD, 
                                                            A.ID, 
                                                            A.USUARIO_ID_MOD
                                                    FROM 	INTERCONSULTA.INTERCONSULTA A
                                                    WHERE 	ID = :oa_id"), 
                                            array(
                                                'oa_id' => $oa_id
                                            ));

                                            $resp_mov_ic = MovEstadoIc::create([
                                                'ESTADO_IC_ID' => 10, 
                                                'FECHA_ESTADO' => now(), 
                                                'USUARIO_IC_ID' => $usuario, 
                                                'ESTABLECIMIENTO_ID' => $resp_ic[0]->estab_origen_id, 
                                                'OBSERVACION' => 'Proceso de asignación de hora por sobrecupo', 
                                                'INTERCONSULTA_ID' => $oa_id
                                            ]); 

                                            $resp_ic = DB::update( DB::raw(
                                                "UPDATE INTERCONSULTA.INTERCONSULTA 
                                                        SET 	ESTADO_IC_ID = :estado_ic, 
                                                                USUARIO_ID_MOD = :usuario,
                                                                FECHA_MOD = :fecha,
                                                                HORAS_ASIGNADAS_ID = :id_horas_gen 
                                                        WHERE ID = :oa_id"), 
                                            array(
                                                'oa_id' => $oa_id,
                                                'id_horas_gen' => $id_hora_generada,
                                                'usuario' => $usuario,
                                                'fecha' => now(),
                                                'estado_ic' => 10
                                            ));


                                            $resp_cts = CuentasCtes::where('ID', $resp_cts->id)
                                                            ->update([
                                                                'oa_id' => $oa_id
                                                            ]);
                                        }
                                    
                                        $object_response['data'][0]['type'] = "Hora Generada (Sobrecupo)";
                                        $object_response['data'][0]['id'] = $resp_hg->id;
                                        $object_response['data'][0]['attributes']['bloque_id'] = $bloque_id;
                                        $object_response['data'][0]['attributes']['fecha'] = $fecha_sc;
                                        $object_response['data'][0]['attributes']['estado_horas_id'] = 7;
                                        $object_response['data'][0]['attributes']['tipo_atencion_e_id'] = $tipo_atencion_id;
                                        $object_response['data'][0]['attributes']['tipo_atencion_p_id'] = $tipo_atencion_id;
                                        $object_response['data'][0]['attributes']['tipo_solicitud_id'] = 7;
                                        $object_response['data'][0]['attributes']['fecha_creacion'] = now();
                                        $object_response['data'][0]['attributes']['fecha_asignada'] = now();
                                        $object_response['data'][0]['attributes']['usuario_id_mod'] = $usuario;
                                        $object_response['data'][0]['attributes']['interconsulta_id'] = @$ic_id;
                                        $object_response['data'][0]['attributes']['oa_id'] = @$oa_id;
                                        $object_response['data'][0]['attributes']['es_sobrecupo'] = 'S';
                                        throw new HttpResponseException(response()->json($object_response, 200));
                                    }

                                }else{
                                    $object_response['errors'][0]['status'] = 400;
                                    $object_response['errors'][0]['source'] = 'SobreCupoController@store';
                                    $object_response['errors'][0]['title'] = 'No se creo sobrecupo';
                                    $object_response['errors'][0]['detail'] = 'Paciente ingresado no posee ficha local';
                                    throw new HttpResponseException(response()->json($object_response, 400));
                                }
                            }
                        }

                    }else{
                        $object_response['errors'][0]['status'] = 400;
                        $object_response['errors'][0]['source'] = 'SobreCupoController@store';
                        $object_response['errors'][0]['title'] = 'No se creo sobrecupo';
                        $object_response['errors'][0]['detail'] = 'Tipo atencion no es valida para el establecimiento al que pertenece el bloque horario';
                        throw new HttpResponseException(response()->json($object_response, 400));
                        
                    }
                }
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'SobreCupoController@store';
                $object_response['errors'][0]['title'] = 'No se creo sobrecupo';
                $object_response['errors'][0]['detail'] = 'El bloque horario ingresado se encuentra inactivo';
                throw new HttpResponseException(response()->json($object_response, 400));
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }              
    }

    /**
     * LISTADO DE SOBRECUPOS ASIGNADOS AL PACIENTE
     */
    public function listadoSobrecuposPaciente(ListarSobrecuposPacientesRequest $object_request){

        $datos_paciente = DB::select( DB::raw(
            "SELECT dlocal.ID	  
                FROM refcentral.DATOS_PACIENTE dpac
                INNER JOIN refcentral.DATOS_LOCALES dlocal ON dpac.ID = dlocal.DATOS_PACIENTE_ID
                WHERE dpac.RUT = :rut
                    AND dlocal.ESTABLECIMIENTO_ID = :establecimiento_id"), 
        array(
            'rut' => $object_request->RUT_PACIENTE,
            'establecimiento_id' => $object_request->ESTABLECIMIENTO_ID
        ));

        if ($datos_paciente){

            $resp_hg_sc = DB::select( DB::raw(
                "SELECT * 
                    FROM HORAS_GENERADAS HG
                    INNER JOIN CUENTAS_CTES CC ON CC.HORAS_GENERADAS_ID = HG.ID
                    WHERE ES_SOBRECUPO = 'S'
                        AND CC.DATOS_LOCALES_ID = :id_paciente"), 
            array(
                'id_paciente' => $datos_paciente[0]->id
            ));

            if ($resp_hg_sc){
                $i = 0;
                $object_response['data'][0]['type'] = "Horas Generadas (Sobrecupo) para paciente consultado";
                $object_response['data'][0]['rut_paciente'] =$object_request->RUT_PACIENTE;
                $object_response['data'][0]['establecimiento_id'] = $object_request->ESTABLECIMIENTO_ID;

                foreach ($resp_hg_sc as $sobrecupo){
                    foreach($sobrecupo as $key => $value){
                        $object_response['data'][$i]['attributes'][$key] = $value;
                    }
                    $i++;
                }
                throw new HttpResponseException(response()->json($object_response, 200));
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'SobreCupoController@listadoSobrecuposPaciente';
                $object_response['errors'][0]['title'] = 'No se encontraron sobrecupos';
                $object_response['errors'][0]['detail'] = 'El paciente ingresado no tiene sobrecupos solicitados para dicho establecimiento';
                throw new HttpResponseException(response()->json($object_response, 400));
            }

        }else{
            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'SobreCupoController@listadoSobrecuposPaciente';
            $object_response['errors'][0]['title'] = 'No se encontraron sobrecupos';
            $object_response['errors'][0]['detail'] = 'No existen registros del paciente en el establecimiento ingresado';
            throw new HttpResponseException(response()->json($object_response, 400));
        }

       



    }

}
