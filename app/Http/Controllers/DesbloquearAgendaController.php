<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Entities\Persona;
use App\Entities\Contrato;
use App\Entities\BloqueHorario;
use App\Entities\JerarquiaEspec;
use App\Entities\Ausencia;
use App\Entities\HorasGeneradas;

use App\Http\Requests\ObtenerContratosProfesionalRequest;
use App\Http\Requests\ObtenerMotivosAusenciaRequest;
use App\Http\Requests\InsertarBloqueoRequest;
use App\Http\Requests\ObtenerBloqueosRequest;
use App\Http\Requests\EliminarBloqueoRequest;

class DesbloquearAgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function obtenerBloqueos(ObtenerBloqueosRequest $request)
    {
        try{
            $contrato  = Contrato::with('persona')
                ->where('CONTRATO.PERSONA_ID', $request->get('PERSONA_ID'))
                ->where('CONTRATO.ID', $request->get('CONTRATO_ID'))
                ->where('CONTRATO.ACTIVO', 'S')
                ->first();

                
            $object_response['data']['persona']['type'] = "Persona";
            $object_response['data']['persona']['attributes']['id'] = $contrato->persona->id;
            $object_response['data']['persona']['attributes']['nombres'] = $contrato->persona->nombres;
            $object_response['data']['persona']['attributes']['apellido_pat'] = $contrato->persona->apellido_pat;
            $object_response['data']['persona']['attributes']['apellido_mat'] = $contrato->persona->apellido_mat;

            $object_response['data']['contrato']['type'] = "Contrato";
            $object_response['data']['contrato']['attributes']['id'] = $contrato->id;  
            $object_response['data']['contrato']['attributes']['Establecimiento_id'] = $contrato->establecimiento_id;           
            $object_response['data']['contrato']['attributes']['Resolucion'] = $contrato->resolucion;
                  

            if(!$contrato)
            {
                $object_response["descripcion"] = "Contrato no se encuentra activo";

                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DesbloquearAgendaController@obtenerBloqueos';
                $object_response['errors'][0]['title'] =  'Contrato inactivo';
                $object_response['errors'][0]['detail'] = "Contrato no se encuentra activo";                
                throw new HttpResponseException(response()->json($object_response, 402));
            }
            else
            {   
                $bloqueos = Ausencia::where('contrato_id', $request->get('CONTRATO_ID'))
                    ->where('USUARIO.ACTIVO', 'S')
                    ->whereDate('FECHA_DESDE', '>', date("Y-m-d H:i:s", strtotime($request->get('FECHA_DESDE'))) )
                    ->join('USUARIO', 'AUSENCIAS.USUARIO_ID_MOD', '=', 'USUARIO.ID')  
                    ->get();

                foreach ($bloqueos as $key => $bloqueo) 
                {
                    $object_response['data']['bloqueo'][$key]['type'] = "Bloqueo";
                    $object_response['data']['bloqueo'][$key]['attributes']['id'] = $bloqueo->id;
                    $object_response['data']['bloqueo'][$key]['attributes']['contrato_id'] = $bloqueo->contrato_id;
                    $object_response['data']['bloqueo'][$key]['attributes']['fecha'] = $bloqueo->fecha;
                    $object_response['data']['bloqueo'][$key]['attributes']['fecha_desde'] = $bloqueo->fecha_desde;
                    $object_response['data']['bloqueo'][$key]['attributes']['fecha_hasta'] = $bloqueo->fecha_hasta;

                    $object_response['data']['bloqueo'][$key]['attributes']['motivo_ausencia_id'] = $bloqueo->motivo_ausencia_id;
                    $object_response['data']['bloqueo'][$key]['attributes']['cantidad_pacientes'] = $bloqueo->cantidad_pacientes;
                    $object_response['data']['bloqueo'][$key]['attributes']['destino_pacient_id'] = $bloqueo->destino_pacient_id;
                    $object_response['data']['bloqueo'][$key]['attributes']['solicitant_bloq_id'] = $bloqueo->solicitant_bloq_id;

                    $object_response['data']['bloqueo'][$key]['attributes']['observacion'] = $bloqueo->observacion;
                    $object_response['data']['bloqueo'][$key]['attributes']['usuario_id_mod'] = $bloqueo->usuario_id_mod;
                    $object_response['data']['bloqueo'][$key]['attributes']['usuario'] = $bloqueo->usuario;
                }

            
                $bloqueos_borrados = Ausencia::where('contrato_id', $request->get('CONTRATO_ID'))
                    ->where('USUARIO.ACTIVO', 'N')
                    ->whereDate('FECHA_DESDE', '>', date("Y-m-d H:i:s", strtotime($request->get('FECHA_DESDE'))) )
                    ->join('USUARIO', 'AUSENCIAS.USUARIO_ID_MOD', '=', 'USUARIO.ID')  
                    ->get();       

                foreach ($bloqueos_borrados as $key => $bloqueo_borrado) 
                {
                    $object_response['data']['bloqueos_borrados'][$key]['type'] = "Bloqueo";
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['id'] = $bloqueo_borrado->id;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['contrato_id'] = $bloqueo_borrado->contrato_id;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['fecha'] = $bloqueo_borrado->fecha;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['fecha_desde'] = $bloqueo_borrado->fecha_desde;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['fecha_hasta'] = $bloqueo_borrado->fecha_hasta;

                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['motivo_ausencia_id'] = $bloqueo_borrado->motivo_ausencia_id;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['cantidad_pacientes'] = $bloqueo_borrado->cantidad_pacientes;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['destino_pacient_id'] = $bloqueo_borrado->destino_pacient_id;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['solicitant_bloq_id'] = $bloqueo_borrado->solicitant_bloq_id;

                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['observacion'] = $bloqueo_borrado->observacion;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['usuario_id_mod'] = $bloqueo_borrado->usuario_id_mod;
                    $object_response['data']['bloqueos_borrados'][$key]['attributes']['usuario'] = $bloqueo_borrado->usuario;
                }
            }


            throw new HttpResponseException(response()->json($object_response, 200));

        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }            
    }

    public function eliminarBloqueo(EliminarBloqueoRequest $request)
    {
        try{
            $ausencia =  Ausencia::find($request->get('AUSENCIA_ID'));
            $ausencia->usuario_id_mod = $request->get('USUARIO_MOD_ID'); 
            $ausencia->fecha_mod = now();
            $ausencia->USUARIO_MOD_ID = $request->get('USUARIO_MOD_ID');
            $ausencia->IP_MOD = $request->get('IP_MOD');
            $ausencia->activo = 'N'; 


            if($ausencia->save())
            {
                $bloques_horarios_id = BloqueHorario::where('CONTRATO.ID',$request->get('CONTRATO_ID'))
                    ->where('CONTRATO.ACTIVO','S')
                    ->where('BLOQUE_HORARIO.ACTIVO','S')            
                    ->join('CONTRATO', 'BLOQUE_HORARIO.CONTRATO_ID', '=', 'CONTRATO.ID')  
                    ->pluck('BLOQUE_HORARIO.id')
                    ->toArray();

                if(!$bloques_horarios_id){

                    $object_response["descripcion"] = "Bloque horario no se encuentra activo";

                    $object_response['errors'][0]['status'] = 400;
                    $object_response['errors'][0]['source'] = 'DesbloquearAgendaController@eliminarBloqueo';
                    $object_response['errors'][0]['title'] =  'Bloque horario inactivo';
                    $object_response['errors'][0]['detail'] = "Bloque horario no se encuentra activo";                
                    throw new HttpResponseException(response()->json($object_response, 402));

                }
                else{
                    $horas_generadas = HorasGeneradas::where('ausencias_id', $request->get('AUSENCIA_ID'))
                    ->where('estado_horas_id', '<>', '9')
                    ->whereIn('bloque_horario_id' , $bloques_horarios_id)
                    ->update(array(
                        'estado_horas_id' => 5,
                        'ausencias_id' => 0,
                    ));                
                }
            }
            else
            {
                $object_response["descripcion"] = "Ausencia no se encuentra activa";

                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DesbloquearAgendaController@eliminarBloqueo';
                $object_response['errors'][0]['title'] =  'Ausencia inactiva';
                $object_response['errors'][0]['detail'] = "Ausencia no se encuentra activa";                
                throw new HttpResponseException(response()->json($object_response, 402));
            }
            
            $object_response["success"] = true;
            $object_response["message"] = "Desbloqueo de agenda realizada con exito";
            $object_response["data"][0]['ausencia_id'] = $request->get('AUSENCIA_ID');
            $object_response["data"][0]['usuario_mod_id'] = $request->get('USUARIO_MOD_ID');
            $object_response["data"][0]['ip_mod'] = $request->get('IP_MOD');

            throw new HttpResponseException(response()->json($object_response, 200));
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }                                                                  
    }    
}
