<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 04/01/2020
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Carbon\Carbon;
use App\Http\Controllers\Helpers\Format;


use App\Entities\Persona;
use App\Entities\Contrato;
use App\Entities\BloqueHorario;
use App\Entities\Ausencia;
use App\Entities\HorasGeneradas;
use App\Entities\EstadoHoras;
use App\Entities\DatosPaciente;
use App\Entities\DatosLocales;
use App\Entities\JerarquiaEspec;
use App\Entities\Policlinicos;
use App\Entities\CuentasCtes;
use App\Entities\AtencionEstab;

use App\Http\Requests\BusquedaRutRequest;
use App\Http\Requests\BusquedaNombreRequest;
use App\Http\Requests\BusquedaFichaRequest;
use App\Http\Requests\ListadoPoliclinicosRequest;
use App\Http\Requests\ListadoProfesionalesRequest;
use App\Http\Requests\ListadoCuposDisponiblesRequest;
use App\Http\Requests\ListadoHorasPorFechaRequest;
use App\Http\Requests\AgendarHoraRequest;
use App\Http\Requests\DetalleHoraRequest;


class DarHoraController extends Controller
{
    
    /**
     * BUSQUEDA PACIENTE POR RUT
     */
    public function busquedaRut(BusquedaRutRequest $request)
    {
        try{

            $resp_paciente = DatosPaciente::where('RUT', $request->RUT_PACIENTE)
                                          ->where('ACTIVO', 'S')->first();
            
            if ($resp_paciente){

                $object_response['data'][0]['type'] = "Datos Paciente";
                $object_response['data'][0]['id'] = $resp_paciente->id;
                $object_response['data'][0]['attributes']['rut'] = $resp_paciente->rut;
                $object_response['data'][0]['attributes']["dv"] = $resp_paciente->dv;
                $object_response['data'][0]['attributes']["nombres"] = $resp_paciente->nombres;
                $object_response['data'][0]['attributes']["apellido_pat"] = $resp_paciente->apellido_pat;
                $object_response['data'][0]['attributes']["apellido_mat"] = $resp_paciente->apellido_mat;
                $object_response['data'][0]['attributes']["sexo_id"] = $resp_paciente->sexo_id;
                $object_response['data'][0]['attributes']["fecha_nac"] = $resp_paciente->fecha_nac;
                $object_response['data'][0]['attributes']["nombre_padre"] = $resp_paciente->nombre_padre;
                $object_response['data'][0]['attributes']["nombre_madre"] = $resp_paciente->nombre_madre;
                $object_response['data'][0]['attributes']["direccion"] = $resp_paciente->direccion;
                $object_response['data'][0]['attributes']["comuna_id"] = $resp_paciente->comuna_id;
                $object_response['data'][0]['attributes']["etnia_id"] = $resp_paciente->etnia_id;
                $object_response['data'][0]['attributes']["etniapercepcion_id"] = $resp_paciente->etniapercepcion_id;
                $object_response['data'][0]['attributes']["estado_civil_id"] = $resp_paciente->estado_civil_id;
                $object_response['data'][0]['attributes']["grupo_sangre_id"] = $resp_paciente->grupo_sangre_id;
                $object_response['data'][0]['attributes']["factor_sangre_id"] = $resp_paciente->factor_sangre_id;
                $object_response['data'][0]['attributes']["nombre_conyuge"] = $resp_paciente->nombre_conyuge;
                $object_response['data'][0]['attributes']["prevision_id"] = $resp_paciente->prevision_id;
                $object_response['data'][0]['attributes']["vencimiento_prev"] = $resp_paciente->vencimiento_prev;
                $object_response['data'][0]['attributes']["fecha_fall"] = $resp_paciente->fecha_fall;
                $object_response['data'][0]['attributes']["activo"] = $resp_paciente->activo;
                $object_response['data'][0]['attributes']["usuario_id_mod"] = $resp_paciente->usuario_id_mod;
                $object_response['data'][0]['attributes']["fecha_mod"] = $resp_paciente->fecha_mod;
                $object_response['data'][0]['attributes']["ur"] = $resp_paciente->ur;
                $object_response['data'][0]['attributes']["nacionalidad_id"] = $resp_paciente->nacionalidad_id;
                $object_response['data'][0]['attributes']["email"] = $resp_paciente->email;
                $object_response['data'][0]['attributes']["ocupacion"] = $resp_paciente->ocupacion;
                $object_response['data'][0]['attributes']["rut_rep_legal"] = $resp_paciente->rut_rep_legal;
                $object_response['data'][0]['attributes']["dv_rep_legal"] = $resp_paciente->dv_rep_legal;
                $object_response['data'][0]['attributes']["representante_legal"] = $resp_paciente->representante_legal;
                $object_response['data'][0]['attributes']["n_identificador_extranjero"] = $resp_paciente->n_identificador_extranjero;
                $object_response['data'][0]['attributes']["ultima_validacion_ws"] = $resp_paciente->ultima_validacion_ws;
                $object_response['data'][0]['attributes']["info_fallecido_ws"] = $resp_paciente->info_fallecido_ws;
                $object_response['data'][0]['attributes']["mv_persona_id"] = $resp_paciente->mv_persona_id;
                $object_response['data'][0]['attributes']["desde_ins"] = $resp_paciente->desde_ins;
                $object_response['data'][0]['attributes']["tb_ocupacion_ng_bdup"] = $resp_paciente->tb_ocupacion_ng_bdup;
                $object_response['data'][0]['attributes']["validado_api_minsal"] = $resp_paciente->validado_api_minsal;
                $object_response['data'][0]['attributes']["desde_up"] = $resp_paciente->desde_up;

                $resp_datosLocales = DatosLocales::with('establecimiento')
                                                ->where('DATOS_PACIENTE_ID', $resp_paciente->id)
                                                ->where('ACTIVO', 'S')->get();
                
                if ($resp_datosLocales){
                    $ind_locales = 1;
                    foreach ($resp_datosLocales as $datolocal){
                        $object_response['data'][$ind_locales]['type'] = "Datos Locales";
                        $object_response['data'][$ind_locales]['id'] = $datolocal->id;
                        $object_response['data'][$ind_locales]['ficha'] = $datolocal->ficha;
                        $object_response['data'][$ind_locales]['establecimiento_id'] = $datolocal->establecimiento->id;
                        $object_response['data'][$ind_locales]['descripcion'] = $datolocal->establecimiento->descripcion;
                        $object_response['data'][$ind_locales]['nombre_estab'] = trim($datolocal->establecimiento->nombre_estab);
                        $object_response['data'][$ind_locales]['attributes']["fecha_admision"] = $datolocal->fecha_admision;
                        $object_response['data'][$ind_locales]['attributes']["direccion"] = $datolocal->direccion;
                        $object_response['data'][$ind_locales]['attributes']["sectores_id"] = $datolocal->sectores_id;
                        $object_response['data'][$ind_locales]['attributes']["fono_casa"] = $datolocal->fono_casa;
                        $object_response['data'][$ind_locales]['attributes']["fono_movil"] = $datolocal->fono_movil;
                        $object_response['data'][$ind_locales]['attributes']["nombre_contacto"] = $datolocal->nombre_contacto;
                        $object_response['data'][$ind_locales]['attributes']["direccion_contacto"] = $datolocal->direccion_contacto;
                        $object_response['data'][$ind_locales]['attributes']["fono_contacto"] = $datolocal->fono_contacto;
                        $object_response['data'][$ind_locales]['attributes']["observacion"] = $datolocal->observacion;
                        $object_response['data'][$ind_locales]['attributes']["activo"] = $datolocal->activo;
                        $object_response['data'][$ind_locales]['attributes']["usuario_id_mod"] = $datolocal->usuario_id_mod;
                        $object_response['data'][$ind_locales]['attributes']["fecha_mod"] = $datolocal->fecha_mod;
                        $object_response['data'][$ind_locales]['attributes']["fecha_ult_act"] = $datolocal->fecha_ult_act;
                        $object_response['data'][$ind_locales]['attributes']["num_carpeta_familiar"] = $datolocal->num_carpeta_familiar;
                        $object_response['data'][$ind_locales]['attributes']["desde_ins"] = $datolocal->desde_ins;

                        $ind_locales++;
                    }
                    throw new HttpResponseException(response()->json($object_response, 200));  
                }else{
                    $object_response['data'][1]['type'] = "Datos Locales";
                    $object_response['data'][1]['detail'] = "Paciente no posee Datos Locales en ningun establecimiento";
                    throw new HttpResponseException(response()->json($object_response, 200));    
                }
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DarHoraController@busquedaRut';
                $object_response['errors'][0]['title'] = 'Rut Paciente';
                $object_response['errors'][0]['detail'] = 'Paciente ingresado no se encuentra activo';
                throw new HttpResponseException(response()->json($object_response, 400));    
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }

        
    }

    /**
     * BUSQUEDA PACIENTE POR NOMBRE
     */
    public function busquedaNombre(BusquedaNombreRequest $request)
    {
        try{
            
            
            if ((!$request->NOMBRES)&&(!$request->APELLIDO_PAT)&&(!$request->APELLIDO_MAT)){
                echo "sin data";
                exit;
            }

            $dataQuery = DatosPaciente::select('id',
                                                    'rut', 
                                                    'dv', 
                                                    'nombres',
                                                    'apellido_pat', 
                                                    'apellido_mat', 
                                                    'sexo_id', 
                                                    'fecha_nac', 
                                                    'email', 
                                                    'ocupacion')->where('ACTIVO', 'S')->limit(300);
            /**
             * SE ESTABLECIO UN LIMITE DE 300 RESULTADOS (PARA INCLUIR INFORMACION DE DATOS LOCALES POR PACIENTE)
             */

            if ($request->NOMBRES){
                $dataQuery = $dataQuery->where('NOMBRES', 'LIKE', "%".strtoupper($request->NOMBRES)."%");
            }
            if ($request->APELLIDO_PAT){
                $dataQuery = $dataQuery->where('APELLIDO_PAT', 'LIKE', "%".strtoupper($request->APELLIDO_PAT)."%");
            }
            if ($request->APELLIDO_MAT){
                $dataQuery = $dataQuery->where('APELLIDO_MAT', 'LIKE', "%".strtoupper($request->APELLIDO_MAT)."%");
            }
            $resp_pacientes = $dataQuery->get();            

            if ($resp_pacientes){
                $ind_paciente = 0;
                foreach($resp_pacientes as $resp_paciente)
                {
                    $object_response['data'][$ind_paciente]['type'] = "Datos Paciente";
                    $object_response['data'][$ind_paciente]['id'] = $resp_paciente->id;
                    $object_response['data'][$ind_paciente]['attributes']['rut'] = $resp_paciente->rut;
                    $object_response['data'][$ind_paciente]['attributes']["dv"] = $resp_paciente->dv;
                    $object_response['data'][$ind_paciente]['attributes']["nombres"] = $resp_paciente->nombres;
                    $object_response['data'][$ind_paciente]['attributes']["apellido_pat"] = $resp_paciente->apellido_pat;
                    $object_response['data'][$ind_paciente]['attributes']["apellido_mat"] = $resp_paciente->apellido_mat;
                    $object_response['data'][$ind_paciente]['attributes']["sexo_id"] = $resp_paciente->sexo_id;
                    $object_response['data'][$ind_paciente]['attributes']["fecha_nac"] = $resp_paciente->fecha_nac;
                    $object_response['data'][$ind_paciente]['attributes']["email"] = $resp_paciente->email;
                    $object_response['data'][$ind_paciente]['attributes']["ocupacion"] = $resp_paciente->ocupacion;               

                    $resp_datosLocales = DatosLocales::where('DATOS_PACIENTE_ID', $resp_paciente->id)
                                                    ->where('ACTIVO', 'S')->get();                    
                    if ($resp_datosLocales){
                        $ind_locales = 0;
                        foreach ($resp_datosLocales as $datolocal){
                            $object_response['data'][$ind_paciente][$ind_locales]['type'] = "Datos Locales";
                            $object_response['data'][$ind_paciente][$ind_locales]['id'] = $datolocal->id;
                            $object_response['data'][$ind_paciente][$ind_locales]['ficha'] = $datolocal->ficha;
                            $object_response['data'][$ind_paciente][$ind_locales]['establecimiento_id'] = $datolocal->establecimiento_id;                        
                            $object_response['data'][$ind_paciente][$ind_locales]['attributes']["fono_casa"] = $datolocal->fono_casa;
                            $object_response['data'][$ind_paciente][$ind_locales]['attributes']["fono_movil"] = $datolocal->fono_movil;                        
                            $ind_locales++;
                        }
                    }else{
                        $object_response['data'][$ind_paciente][$ind_locales]['type'] = "Datos Locales";
                        $object_response['data'][$ind_paciente][$ind_locales]['detail'] = "Paciente no posee Datos Locales en ningun establecimiento";
                    }
                    
                    $ind_paciente++;
                }
                throw new HttpResponseException(response()->json($object_response, 200));    
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DarHoraController@busquedaRut';
                $object_response['errors'][0]['title'] = 'Busqueda por nombre Paciente';
                $object_response['errors'][0]['detail'] = 'No se encontraron pacientes con el nombre ingresado';
                throw new HttpResponseException(response()->json($object_response, 400));    
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }

    }
    
    /**
     * BUSQUEDA PACIENTE POR N° FICHA
     */
    public function busquedaFicha(BusquedaFichaRequest $request)
    {
        try
        {
            $datolocal = DatosLocales::with('establecimiento')
                                            ->where('FICHA', $request->NUM_FICHA)
                                            ->where('ACTIVO', 'S')->first();

            if ($datolocal->datos_paciente_id){

                $resp_paciente = DatosPaciente::where('ID', $datolocal->datos_paciente_id)
                                        ->where('ACTIVO', 'S')->first();
                if ($resp_paciente){
                    $object_response['data'][0]['type'] = "Datos Paciente";
                    $object_response['data'][0]['id'] = $resp_paciente->id;
                    $object_response['data'][0]['attributes']['rut'] = $resp_paciente->rut;
                    $object_response['data'][0]['attributes']["dv"] = $resp_paciente->dv;
                    $object_response['data'][0]['attributes']["nombres"] = $resp_paciente->nombres;
                    $object_response['data'][0]['attributes']["apellido_pat"] = $resp_paciente->apellido_pat;
                    $object_response['data'][0]['attributes']["apellido_mat"] = $resp_paciente->apellido_mat;
                    $object_response['data'][0]['attributes']["sexo_id"] = $resp_paciente->sexo_id;
                    $object_response['data'][0]['attributes']["fecha_nac"] = $resp_paciente->fecha_nac;
                    $object_response['data'][0]['attributes']["nombre_padre"] = $resp_paciente->nombre_padre;
                    $object_response['data'][0]['attributes']["nombre_madre"] = $resp_paciente->nombre_madre;
                    $object_response['data'][0]['attributes']["direccion"] = $resp_paciente->direccion;
                    $object_response['data'][0]['attributes']["comuna_id"] = $resp_paciente->comuna_id;
                    $object_response['data'][0]['attributes']["etnia_id"] = $resp_paciente->etnia_id;
                    $object_response['data'][0]['attributes']["etniapercepcion_id"] = $resp_paciente->etniapercepcion_id;
                    $object_response['data'][0]['attributes']["estado_civil_id"] = $resp_paciente->estado_civil_id;
                    $object_response['data'][0]['attributes']["grupo_sangre_id"] = $resp_paciente->grupo_sangre_id;
                    $object_response['data'][0]['attributes']["factor_sangre_id"] = $resp_paciente->factor_sangre_id;
                    $object_response['data'][0]['attributes']["nombre_conyuge"] = $resp_paciente->nombre_conyuge;
                    $object_response['data'][0]['attributes']["prevision_id"] = $resp_paciente->prevision_id;
                    $object_response['data'][0]['attributes']["vencimiento_prev"] = $resp_paciente->vencimiento_prev;
                    $object_response['data'][0]['attributes']["fecha_fall"] = $resp_paciente->fecha_fall;
                    $object_response['data'][0]['attributes']["activo"] = $resp_paciente->activo;
                    $object_response['data'][0]['attributes']["usuario_id_mod"] = $resp_paciente->usuario_id_mod;
                    $object_response['data'][0]['attributes']["fecha_mod"] = $resp_paciente->fecha_mod;
                    $object_response['data'][0]['attributes']["ur"] = $resp_paciente->ur;
                    $object_response['data'][0]['attributes']["nacionalidad_id"] = $resp_paciente->nacionalidad_id;
                    $object_response['data'][0]['attributes']["email"] = $resp_paciente->email;
                    $object_response['data'][0]['attributes']["ocupacion"] = $resp_paciente->ocupacion;
                    $object_response['data'][0]['attributes']["rut_rep_legal"] = $resp_paciente->rut_rep_legal;
                    $object_response['data'][0]['attributes']["dv_rep_legal"] = $resp_paciente->dv_rep_legal;
                    $object_response['data'][0]['attributes']["representante_legal"] = $resp_paciente->representante_legal;
                    $object_response['data'][0]['attributes']["n_identificador_extranjero"] = $resp_paciente->n_identificador_extranjero;
                    $object_response['data'][0]['attributes']["ultima_validacion_ws"] = $resp_paciente->ultima_validacion_ws;
                    $object_response['data'][0]['attributes']["info_fallecido_ws"] = $resp_paciente->info_fallecido_ws;
                    $object_response['data'][0]['attributes']["mv_persona_id"] = $resp_paciente->mv_persona_id;
                    $object_response['data'][0]['attributes']["desde_ins"] = $resp_paciente->desde_ins;
                    $object_response['data'][0]['attributes']["tb_ocupacion_ng_bdup"] = $resp_paciente->tb_ocupacion_ng_bdup;
                    $object_response['data'][0]['attributes']["validado_api_minsal"] = $resp_paciente->validado_api_minsal;
                    $object_response['data'][0]['attributes']["desde_up"] = $resp_paciente->desde_up;


                    $object_response['data'][1]['type'] = "Datos Locales (Ficha Consultada)";
                    $object_response['data'][1]['id'] = $datolocal->id;
                    $object_response['data'][1]['ficha'] = $datolocal->ficha;
                    $object_response['data'][1]['establecimiento_id'] = $datolocal->establecimiento->id;
                    $object_response['data'][1]['descripcion'] = $datolocal->establecimiento->descripcion;
                    $object_response['data'][1]['nombre_estab'] = trim($datolocal->establecimiento->nombre_estab);
                    $object_response['data'][1]['attributes']["fecha_admision"] = $datolocal->fecha_admision;
                    $object_response['data'][1]['attributes']["direccion"] = $datolocal->direccion;
                    $object_response['data'][1]['attributes']["sectores_id"] = $datolocal->sectores_id;
                    $object_response['data'][1]['attributes']["fono_casa"] = $datolocal->fono_casa;
                    $object_response['data'][1]['attributes']["fono_movil"] = $datolocal->fono_movil;
                    $object_response['data'][1]['attributes']["nombre_contacto"] = $datolocal->nombre_contacto;
                    $object_response['data'][1]['attributes']["direccion_contacto"] = $datolocal->direccion_contacto;
                    $object_response['data'][1]['attributes']["fono_contacto"] = $datolocal->fono_contacto;
                    $object_response['data'][1]['attributes']["observacion"] = $datolocal->observacion;
                    $object_response['data'][1]['attributes']["activo"] = $datolocal->activo;
                    $object_response['data'][1]['attributes']["usuario_id_mod"] = $datolocal->usuario_id_mod;
                    $object_response['data'][1]['attributes']["fecha_mod"] = $datolocal->fecha_mod;
                    $object_response['data'][1]['attributes']["fecha_ult_act"] = $datolocal->fecha_ult_act;
                    $object_response['data'][1]['attributes']["num_carpeta_familiar"] = $datolocal->num_carpeta_familiar;
                    $object_response['data'][1]['attributes']["desde_ins"] = $datolocal->desde_ins;

                    throw new HttpResponseException(response()->json($object_response, 200)); 
                }else{
                    $object_response['errors'][0]['status'] = 400;
                    $object_response['errors'][0]['source'] = 'DarHoraController@busquedaFicha';
                    $object_response['errors'][0]['title'] = 'Rut Paciente';
                    $object_response['errors'][0]['detail'] = 'Paciente ingresado no se encuentra activo';
                    throw new HttpResponseException(response()->json($object_response, 400));    
                }
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DarHoraController@busquedaFicha';
                $object_response['errors'][0]['title'] = 'Numero Ficha Paciente';
                $object_response['errors'][0]['detail'] = 'Ficha Paciente ingresada no se encuentra activo';
                throw new HttpResponseException(response()->json($object_response, 400));  
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }
    
    /**
     * LISTADO POLICLINICOS SISTEMA 
     */
    public function listadoPoliclinicos(ListadoPoliclinicosRequest $request)
    {
        try
        {
            $policlinicos = JerarquiaEspec::with('policlinico')->where('ACTIVO', 'S')->where('ESTABLECIMIENTO_ID', $request->ESTABLECIMIENTO_ID)->get();
            
            if($policlinicos){
                $ind_pol = 0;
                foreach($policlinicos as $policlinico){
                    $object_response['data'][$ind_pol]['type'] = "Policlinico (Especialidad)";
                    $object_response['data'][$ind_pol]['establecimiento_id'] = $policlinico->establecimiento_id;
                    $object_response['data'][$ind_pol]['jerarquia_espec'] = $policlinico->id;
                    $object_response['data'][$ind_pol]['policlinico(especialidad)'] = $policlinico->policlinico->descripcion;
                    $object_response['data'][$ind_pol]['especialidades_id'] = $policlinico->especialidades_id;
                    $object_response['data'][$ind_pol]['attributes']['subespecialidad_id'] = $policlinico->subespecialidad_id;
                    $object_response['data'][$ind_pol]['attributes']['agrupaciones_id'] = $policlinico->agrupaciones_id;
                    $object_response['data'][$ind_pol]['attributes']['policlinicos_id'] = $policlinico->policlinicos_id;
                    $object_response['data'][$ind_pol]['attributes']['edad_minima'] = $policlinico->edad_minima;
                    $object_response['data'][$ind_pol]['attributes']['edad_maxima'] = $policlinico->edad_maxima;
                    $object_response['data'][$ind_pol]['attributes']['sexo_id'] = $policlinico->sexo_id;
                    $object_response['data'][$ind_pol]['attributes']['ubicacion'] = $policlinico->ubicacion;
                    $object_response['data'][$ind_pol]['attributes']['mensaje'] = $policlinico->mensaje;
                    $object_response['data'][$ind_pol]['attributes']['nivel'] = $policlinico->nivel;
                    $object_response['data'][$ind_pol]['attributes']['unidad_atencion_id'] = $policlinico->unidad_atencion_id;
                    $object_response['data'][$ind_pol]['attributes']['usuario_crea_id'] = $policlinico->usuario_crea_id;
                    $object_response['data'][$ind_pol]['attributes']['fecha_crea'] = $policlinico->fecha_crea;
                    $object_response['data'][$ind_pol]['attributes']['ip_crea'] = $policlinico->ip_crea;
                    $object_response['data'][$ind_pol]['attributes']['usuario_mod_id'] = $policlinico->usuario_mod_id;
                    $object_response['data'][$ind_pol]['attributes']['fecha_mod'] = $policlinico->fecha_mod;
                    $object_response['data'][$ind_pol]['attributes']['ip_mod'] = $policlinico->ip_mod;
                    $object_response['data'][$ind_pol]['attributes']['tb_modulo_origen_mod_id'] = $policlinico->tb_modulo_origen_mod_id;
                    $object_response['data'][$ind_pol]['attributes']['tb_modulo_origen_crea_id'] = $policlinico->tb_modulo_origen_crea_id;
                    $object_response['data'][$ind_pol]['attributes']['visible'] = $policlinico->visible;
                    $object_response['data'][$ind_pol]['attributes']['activo'] = $policlinico->activo;
                    
                    $ind_pol++;
                }
                throw new HttpResponseException(response()->json($object_response, 200));
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DarHoraController@listadoPoliclinicos';
                $object_response['errors'][0]['title'] = 'Policlinicos por establecimiento';
                $object_response['errors'][0]['detail'] = 'No se encontraron policlinicos para establecimiento ingresado';
                throw new HttpResponseException(response()->json($object_response, 400)); 
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }

    /**
     * LISTADO PROFESIONALES POR POLICLINICO
     */
    public function listadoProfesionales(ListadoProfesionalesRequest $request)
    {
       // $resp_contratos = Contrato::select('PERSONA_ID')->where('ACTIVO', 'S')->where('ESTABLECIMIENTO_ID', $request->ESTABLECIMIENTO_ID)->groupBy('PERSONA_ID')->get();
        
        $resp_especialidad = JerarquiaEspec::with('policlinico')->where('ID', $request->JERARQUIA_ESPEC_ID)->first();

        $resp_contratos = Contrato::select('PERSONA_ID')->where('ACTIVO', 'S')->where('ESPECIALIDADES_ID', $resp_especialidad->especialidades_id)->groupBy('PERSONA_ID')->get();
        
        if ($resp_contratos){

            $ind_pers = 1;
            $object_response['data'][0]['type'] = "Profesionales por policlinico (especialidades)";
            $object_response['data'][0]['jerarquia_espec_id'] = $request->JERARQUIA_ESPEC_ID;
            $object_response['data'][0]['establecimiento_id'] = $resp_especialidad->establecimiento_id;
            $object_response['data'][0]['especialidades_id'] = $resp_especialidad->especialidades_id;
            $object_response['data'][0]['descripcion'] = $resp_especialidad->policlinico->descripcion;
            foreach($resp_contratos as $contrato){
            
                $resp_persona = Persona::where('ID', $contrato->persona_id)->where('ACTIVO', 'S')->first();
                
                $object_response['data'][$ind_pers]['attributes']["id"] = $resp_persona->id;
                $object_response['data'][$ind_pers]['attributes']["rut"] = $resp_persona->rut;
                $object_response['data'][$ind_pers]['attributes']["dv"] = $resp_persona->dv;
                $object_response['data'][$ind_pers]['attributes']["nombres"] = $resp_persona->nombres;
                $object_response['data'][$ind_pers]['attributes']["apellido_pat"] = $resp_persona->apellido_pat;
                $object_response['data'][$ind_pers]['attributes']["apellido_mat"] = $resp_persona->apellido_mat;
                $object_response['data'][$ind_pers]['attributes']["sexo_id"] = $resp_persona->sexo_id;
                $object_response['data'][$ind_pers]['attributes']["direccion"] = $resp_persona->direccion;
                $object_response['data'][$ind_pers]['attributes']["estado_civil_id"] = $resp_persona->estado_civil_id;
                $object_response['data'][$ind_pers]['attributes']["comunas_id"] = $resp_persona->comunas_id;
                $object_response['data'][$ind_pers]['attributes']["fono_casa"] = $resp_persona->fono_casa;
                $object_response['data'][$ind_pers]['attributes']["fono_oficina"] = $resp_persona->fono_oficina;
                $object_response['data'][$ind_pers]['attributes']["fono_movil"] = $resp_persona->fono_movil;
                $object_response['data'][$ind_pers]['attributes']["email"] = $resp_persona->email;
                $object_response['data'][$ind_pers]['attributes']["activo"] = $resp_persona->activo;
                $object_response['data'][$ind_pers]['attributes']["usuario_id_mod"] = $resp_persona->usuario_id_mod;
                $object_response['data'][$ind_pers]['attributes']["fecha_mod"] = $resp_persona->fecha_mod;
                $object_response['data'][$ind_pers]['attributes']["imei1"] = $resp_persona->imei1;
                $object_response['data'][$ind_pers]['attributes']["imei2"] = $resp_persona->imei2;
                $object_response['data'][$ind_pers]['attributes']["desde_ins"] = $resp_persona->desde_ins;
                $object_response['data'][$ind_pers]['attributes']["validado_api_minsal"] = $resp_persona->validado_api_minsal;
                $object_response['data'][$ind_pers]['attributes']["fecha_nacimiento"] = $resp_persona->fecha_nacimiento;

                $ind_pers++;
            }

            throw new HttpResponseException(response()->json($object_response, 200));
        }else{
            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'DarHoraController@listadoProfesionales';
            $object_response['errors'][0]['title'] = 'Listado Profesionales';
            $object_response['errors'][0]['detail'] = 'No se encontraron profesionales activos para policlinico (especialidad)';
            throw new HttpResponseException(response()->json($object_response, 400));
        }
    }


    /**
     * LISTADO CUPOS DISPONIBLES 
     */
    public function listadoCuposDisponibles(ListadoCuposDisponiblesRequest $request)
    {
        $total_libres = 0;
        $total_asignadas = 0;
        $resp_bh = BloqueHorario::with('jerarquia_espec')->with('contrato')->where('ACTIVO', 'S')->where('jerarquia_espec_id', $request->JERARQUIA_ESPEC_ID)->get();
                       
        if (@$resp_bh[0]->id)
        {           
            $policlinico = Policlinicos::select('descripcion')->where('ID', $resp_bh[0]->jerarquia_espec->policlinicos_id)->first();
            $object_response['data'][0]['type'] = "Horas Disponibles";
            $object_response['data'][0]['jerarquia_espec_id'] = $request->JERARQUIA_ESPEC_ID;;
            $object_response['data'][0]['policlinico (especialidad)'] = $policlinico->descripcion;
            $object_response['data'][0]['fecha_inicio'] = $request->FECHA_INICIO;
            $object_response['data'][0]['fecha_fin'] = $request->FECHA_FIN;

            $ind_bh = 1;
            foreach($resp_bh as $bloque_horario)
            {
                $persona = Persona::where('ID', $bloque_horario->contrato->persona_id)->where('ACTIVO', 'S')->first();



                $resp_horas = HorasGeneradas::where('BLOQUE_HORARIO_ID', $bloque_horario->id)
                                            ->whereBetween('FECHA', [ date("Y/m/d h:s", strtotime($request->FECHA_INICIO)), date("Y/m/d h:s", strtotime($request->FECHA_FIN))])
                                            ->whereIn('ESTADO_HORAS_ID',[5])
                                            ->get();
                $ind_hg = 0;                                            
                if (@$resp_horas[0]->id)
                {   
                    $object_response['data'][$ind_bh]['type'] = "Bloque Horario";
                    $object_response['data'][$ind_bh]['id'] = $bloque_horario->id;
                    $object_response['data'][$ind_bh]['attributes']['contrato_id'] = $bloque_horario->contrato_id;
                    $object_response['data'][$ind_bh]['attributes']['dia'] = $bloque_horario->dia;
                    $object_response['data'][$ind_bh]['attributes']['nombre_dia'] = Format::nombreDias($bloque_horario->dia);
                    $object_response['data'][$ind_bh]['attributes']['hora_inicio'] = $bloque_horario->hora_inicio;
                    $object_response['data'][$ind_bh]['attributes']['hora_fin'] = $bloque_horario->hora_fin;
                    $object_response['data'][$ind_bh]['attributes']['persona_id'] = $bloque_horario->contrato->persona_id;
                    $object_response['data'][$ind_bh]['attributes']['rut'] = $persona->rut;
                    $object_response['data'][$ind_bh]['attributes']['dv'] = $persona->dv;
                    $object_response['data'][$ind_bh]['attributes']['nombres'] = $persona->nombres;
                    $object_response['data'][$ind_bh]['attributes']['apellido_pat'] = $persona->apellido_pat;
                    $object_response['data'][$ind_bh]['attributes']['apellido_mat'] = $persona->apellido_mat;

                    foreach($resp_horas as $hora)
                    {   

                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['type'] = "Hora Generada";
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['id'] = $hora->id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['bloque_horario_id'] = $hora->bloque_horario_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['fecha'] = $hora->fecha;
                        
                        $estado_hora = EstadoHoras::select('descripcion')->where('ID', $hora->estado_horas_id)->first();
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['estado_horas_id'] = $hora->estado_horas_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['estado_hora'] =  $estado_hora->descripcion;

                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['tipo_atencion_e_id'] = $hora->tipo_atencion_e_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['tipo_atencion_p_id'] = $hora->tipo_atencion_p_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['tipo_solicitud_id'] = $hora->tipo_solicitud_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['fecha_creacion'] = $hora->fecha_creacion;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['fecha_asignada'] = $hora->fecha_asignada;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['usuario_id_mod'] = $hora->usuario_id_mod;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['orden'] = $hora->orden;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['ausencias_id'] = $hora->ausencias_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['interconsulta_id'] = $hora->interconsulta_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['oa_id'] = $hora->oa_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['es_sobrecupo'] = $hora->es_sobrecupo;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['visible'] = $hora->visible;
                        $total_libres++;
                        $ind_hg++; 
                    }
                    $ind_bh++;
                }
            }
            $object_response['data'][0]['total_disponibles'] = $total_libres;
            throw new HttpResponseException(response()->json($object_response, 200));
        }else{
            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'DarHoraController@listadoCuposDisponibles';
            $object_response['errors'][0]['title'] = 'Listado Horas tomadas y disponibles';
            $object_response['errors'][0]['detail'] = 'No se encontraron horas para la especialidad ingresada';
            throw new HttpResponseException(response()->json($object_response, 400));
        }
            
    }
    
    /**
     * LISTADO HORAS TOMADAS Y DISPONIBLES, DATOS PACIENTES POR FECHA DADA
     */
    public function listadoHorasPorFecha(ListadoHorasPorFechaRequest $request)
    {
        $total_libres = 0;
        $total_asignadas = 0;
        $resp_bh = BloqueHorario::with('jerarquia_espec')->with('contrato')->where('ACTIVO', 'S')->where('jerarquia_espec_id', $request->JERARQUIA_ESPEC_ID)->get();
                       
        if (@$resp_bh[0]->id)
        {           
            $policlinico = Policlinicos::select('descripcion')->where('ID', $resp_bh[0]->jerarquia_espec->policlinicos_id)->first();
            $object_response['data'][0]['type'] = "Horas Disponibles y tomadas (con datos paciente) por fecha";
            $object_response['data'][0]['jerarquia_espec_id'] = $request->JERARQUIA_ESPEC_ID;;
            $object_response['data'][0]['policlinico (especialidad)'] = $policlinico->descripcion;
            $object_response['data'][0]['fecha_inicio'] = $request->FECHA_INICIO;
            $object_response['data'][0]['fecha_fin'] = $request->FECHA_FIN;

            $ind_bh = 1;
            foreach($resp_bh as $bloque_horario)
            {
                $persona = Persona::where('ID', $bloque_horario->contrato->persona_id)->where('ACTIVO', 'S')->first();



                $resp_horas = HorasGeneradas::where('BLOQUE_HORARIO_ID', $bloque_horario->id)
                                            ->whereBetween('FECHA', [ date("Y/m/d h:s", strtotime($request->FECHA_INICIO)), date("Y/m/d h:s", strtotime($request->FECHA_FIN))])
                                            ->whereIn('ESTADO_HORAS_ID',[ 1])
                                            ->get();
                $ind_hg = 0;                                            
                if (@$resp_horas[0]->id)
                {   
                    $object_response['data'][$ind_bh]['type'] = "Bloque Horario";
                    $object_response['data'][$ind_bh]['id'] = $bloque_horario->id;
                    $object_response['data'][$ind_bh]['attributes']['contrato_id'] = $bloque_horario->contrato_id;
                    $object_response['data'][$ind_bh]['attributes']['dia'] = $bloque_horario->dia;
                    $object_response['data'][$ind_bh]['attributes']['nombre_dia'] = Format::nombreDias($bloque_horario->dia);
                    $object_response['data'][$ind_bh]['attributes']['hora_inicio'] = $bloque_horario->hora_inicio;
                    $object_response['data'][$ind_bh]['attributes']['hora_fin'] = $bloque_horario->hora_fin;
                    $object_response['data'][$ind_bh]['attributes']['persona_id'] = $bloque_horario->contrato->persona_id;
                    $object_response['data'][$ind_bh]['attributes']['rut'] = $persona->rut;
                    $object_response['data'][$ind_bh]['attributes']['dv'] = $persona->dv;
                    $object_response['data'][$ind_bh]['attributes']['nombres'] = $persona->nombres;
                    $object_response['data'][$ind_bh]['attributes']['apellido_pat'] = $persona->apellido_pat;
                    $object_response['data'][$ind_bh]['attributes']['apellido_mat'] = $persona->apellido_mat;

                    foreach($resp_horas as $hora)
                    {   

                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['type'] = "Hora Generada";
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['id'] = $hora->id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['bloque_horario_id'] = $hora->bloque_horario_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['fecha'] = $hora->fecha;
                        
                        $estado_hora = EstadoHoras::select('descripcion')->where('ID', $hora->estado_horas_id)->first();
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['estado_horas_id'] = $hora->estado_horas_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['estado_hora'] =  $estado_hora->descripcion;

                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['tipo_atencion_e_id'] = $hora->tipo_atencion_e_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['tipo_atencion_p_id'] = $hora->tipo_atencion_p_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['tipo_solicitud_id'] = $hora->tipo_solicitud_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['fecha_creacion'] = $hora->fecha_creacion;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['fecha_asignada'] = $hora->fecha_asignada;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['usuario_id_mod'] = $hora->usuario_id_mod;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['orden'] = $hora->orden;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['ausencias_id'] = $hora->ausencias_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['interconsulta_id'] = $hora->interconsulta_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['oa_id'] = $hora->oa_id;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['es_sobrecupo'] = $hora->es_sobrecupo;
                        $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['attributes']['visible'] = $hora->visible;
                        if ($hora->estado_horas_id == 1){
                            $total_asignadas++;
                            $ctas_ctes = CuentasCtes::where('HORAS_GENERADAS_ID', $hora->id)->first();

                            if (@$ctas_ctes->datos_locales_id)
                            {
                                $datos_paciente = DatosLocales::where('ID', $ctas_ctes->datos_locales_id)->first();
                            
                                $paciente = DatosPaciente::where('ID', $datos_paciente->datos_paciente_id)->first();
                                
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['id'] = $paciente->id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['rut'] = $paciente->rut;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['dv'] = $paciente->dv;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['nombres'] = $paciente->nombres;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['apellido_pat'] = $paciente->apellido_pat;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['apellido_mat'] = $paciente->apellido_mat;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['sexo_id'] = $paciente->sexo_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['fecha_nac'] = $paciente->fecha_nac;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['nombre_padre'] = $paciente->nombre_padre;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['nombre_madre'] = $paciente->nombre_madre;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['direccion'] = $paciente->direccion;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['comuna_id'] = $paciente->comuna_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['etnia_id'] = $paciente->etnia_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['etniapercepcion_id'] = $paciente->etniapercepcion_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['estado_civil_id'] = $paciente->estado_civil_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['grupo_sangre_id'] = $paciente->grupo_sangre_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['factor_sangre_id'] = $paciente->factor_sangre_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['nombre_conyuge'] = $paciente->nombre_conyuge;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['prevision_id'] = $paciente->prevision_id;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['vencimiento_prev'] = $paciente->vencimiento_prev;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['fecha_fall'] = $paciente->fecha_fall;
                                                                
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['ficha'] = $datos_paciente->ficha;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['fono_casa'] = $datos_paciente->fono_casa;
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente']['fono_movil'] = $datos_paciente->fono_movil;
                            }else{
                                $object_response["data"][$ind_bh]['attributes']['hora_generada'][$ind_hg]['paciente'] = "No se encontro informacion del paciente";
                            }
                        }else{
                            $total_libres++;
                        }
                        $ind_hg++; 
                    }
                    $ind_bh++;
                }
            }
            $object_response['data'][0]['total_asignadas'] = $total_asignadas;
            $object_response['data'][0]['total_libres'] = $total_libres;
            throw new HttpResponseException(response()->json($object_response, 200));
        }else{
            $object_response['errors'][0]['status'] = 400;
            $object_response['errors'][0]['source'] = 'DarHoraController@listadoCuposDisponibles';
            $object_response['errors'][0]['title'] = 'Listado Horas tomadas y disponibles';
            $object_response['errors'][0]['detail'] = 'No se encontraron horas para la especialidad ingresada';
            throw new HttpResponseException(response()->json($object_response, 400));
        }
    }
    
    /**
     * AGENDAR HORA
     */
    public function agendarHora(AgendarHoraRequest $object_request)
    {
        try
        {
            $rut_paciente = $object_request->RUT_PACIENTE;
            $bloque_id = $object_request->BLOQUE_HORARIO_ID;
            $tipo_atencion_id = $object_request->TIPO_ATENCION_ID;
            $fechaCarbon =  Carbon::parse($object_request->FECHA); 
            $fecha = date("Y/m/d H:i", strtotime($object_request->FECHA." ".$object_request->HORA));
            $dia = $fechaCarbon->isoFormat('d');
            $ic_id = $object_request->IC_ID;
            $oa_id = $object_request->OA_ID;
            $usuario = $object_request->USUARIO_CREA_ID;
            $ip_crea = $object_request->IP_CREA;

            /* VERIFICO BLOQUE HORARIO ACTIVO Y OBTENGO ESTABLECIMIENTO Y POLICLINICO */
            $resp_bh = BloqueHorario::with('contrato')
                                    ->where('ID', $bloque_id )
                                    ->where('ACTIVO','S')->first();

        // dd($resp_bh);
            if ($resp_bh)
            {
                $estab_id = $resp_bh->contrato->establecimiento_id;
                $jerarquia_espec_id = $resp_bh->jerarquia_espec_id;
                $dia_bh = $resp_bh->dia;
                echo $dia;
                echo " - ";
                echo $dia_bh;
                if ($dia != $dia_bh){
                    $object_response['errors'][0]['status'] = 400;
                    $object_response['errors'][0]['source'] = 'DarHoraController@agendarHora';
                    $object_response['errors'][0]['title'] = 'No se agendo hora';
                    $object_response['errors'][0]['detail'] = 'Fecha no se corresponde con dia de bloque horario ingresado';
                    throw new HttpResponseException(response()->json($object_response, 400));
                }else{

                    /* VERIFICO TIPO DE ATENCION->ESTABLECIMIENTO DE BLOQUE HORARIO */
                    $resp_tipo_act = AtencionEstab::with('tipo_atencion')
                                                ->where('TIPO_ATENCION_ID', $tipo_atencion_id)
                                                ->where('ESTABLECIMIENTO_ID', $estab_id)->first();
                    if ($resp_tipo_act)
                    {
                        $nombre_tipo_atencion = $resp_tipo_act->tipo_atencion->descripcion;

                        /* VERIFICO EXISTENCIA DE PACIENTE */
                        $resp_paciente = DatosPaciente::where('RUT', $rut_paciente)
                                                    ->where('ACTIVO', 'S')->first();
                        if ($resp_paciente)
                        {
                            /* OBTENGO NOMBRE POLICLINICO */
                            $resp_poli = JerarquiaEspec::with('policlinico')
                                                    ->where('ID', $jerarquia_espec_id)
                                                    ->first();
                            $policlinico = $resp_poli->policlinico->descripcion;
                            
                            /* SE CREA HORA */
                            $valido_hora = HorasGeneradas::where('BLOQUE_HORARIO_ID', $bloque_id)
                                                        ->where('FECHA', $fecha)->first();
                            if ($valido_hora)
                            {
                                $object_response['errors'][0]['status'] = 400;
                                $object_response['errors'][0]['source'] = 'DarHoraController@agendarHora';
                                $object_response['errors'][0]['title'] = 'No se agendo hora';
                                $object_response['errors'][0]['detail'] = 'La hora ingresada ya se encuentra registrada';
                                throw new HttpResponseException(response()->json($object_response, 400));
                            }
                            else
                            {

                                $datos_paciente = DB::select( DB::raw(
                                    "SELECT dlocal.ID	  
                                        FROM refcentral.DATOS_PACIENTE dpac
                                        INNER JOIN refcentral.DATOS_LOCALES dlocal ON dpac.ID = dlocal.DATOS_PACIENTE_ID
                                        WHERE dpac.RUT = :rut
                                            AND dlocal.ESTABLECIMIENTO_ID = :establecimiento_id"), 
                                array(
                                    'rut' => $rut_paciente,
                                    'establecimiento_id' => $estab_id
                                ));
                                
                                if ($datos_paciente){

                                    $resp_hg = HorasGeneradas::create([
                                        'BLOQUE_HORARIO_ID' => $bloque_id,
                                        'FECHA' => $fecha,
                                        'ESTADO_HORAS_ID' => 7,
                                        'TIPO_ATENCION_E_ID' => $tipo_atencion_id,
                                        'TIPO_ATENCION_P_ID' => $tipo_atencion_id,
                                        'TIPO_SOLICITUD_ID' => 7,
                                        'FECHA_CREACION' => now(),
                                        'FECHA_ASIGNADA' => now(),
                                        'USUARIO_ID_MOD' => $usuario,
                                        'INTERCONSULTA_ID' => $ic_id,
                                        'OA_ID' => $oa_id
                                    ]);

                                    if($resp_hg){
                                        $id_hora_generada = $resp_hg->id;
                                        
                                        $resp_cts = CuentasCtes::create([
                                                'datos_locales_id' => $datos_paciente[0]->id,
                                                'horas_generadas_id' => $id_hora_generada,
                                                'documento_anterior' => 0,
                                                'tipo_documento_id' => 1,
                                                'activo' => 'S',
                                                'estado_ctacte_id' => 1,
                                                'fecha_creacion' => now(),
                                                'usuario_crea_id' => $usuario,
                                                'interconsulta_id' => $ic_id,
                                                'oa_id' => $oa_id,
                                                'ip_crea' => $ip_crea
                                        ]);
                                        
                                        if ($ic_id)
                                        {
                                            $resp_ic = Interconsulta::where('ID', $ic_id)->first();

                                            $resp_ic_mov = InterconsultaMovs::create([
                                                'interconsulta_id' => $ic_id,
                                                'fecha' => now(),
                                                'estab_destino_id' => $resp_ic->estab_destino_id,
                                                'jerarquia_dest_id' => $resp_ic->jerarquia_dest_id,
                                                'tipo_derivacion_id' => $resp_ic->tipo_derivacion_id,
                                                'estado_ic_id' => 8,
                                                'estado_auge_id' => $resp_ic->estado_auge_id,
                                                'alerta_id' => $resp_ic->alerta_id,
                                                'diag_cie10_1_id' => $resp_ic->diag_cie10_1_id,
                                                'diag_cie10_2_id' => $resp_ic->diag_cie10_2_id,
                                                'rgs_id' =>$resp_ic->rgs_id,
                                                'estab_origen_id' => $resp_ic->estab_origen_id,
                                                'motivo_interconsulta_id' => $resp_ic->motivo_interconsulta_id,
                                                'usuario_id_mod' => $usuario,
                                                'observacion' => 'CREADA POR EL USUARIO',
                                                'estado_ifl_id' => $resp_ic->estado_ifl_id,
                                                'procedencia_id' => $resp_ic->procedencia_id,
                                                'n_documento_procedencia' => $resp_ic->n_documento_procedencia
                                            ]);

                                            $resp_ic_hrs = InterconsultaHoras::create([
                                                'interconsulta_id' => $ic_id,
                                                'horas_asignadas_id' => $id_hora_generada,
                                                'activo' => 'S',
                                                'usuario_id_mod' => $usuario,
                                                'fecha_mod' => now(),
                                                'estado_ifl_id' => 0,
                                                'usuario_crea_id' => $usuario,
                                                'fecha_crea' => now(),
                                                'ip_crea' => $ip_crea,
                                                'estado_hora_ic_id' => 1,
                                                'establecimiento_que_agenda_id' => $estab_id,
                                                'usuario_que_agenda_id' => $usuario,
                                                'fecha_agendamiento' => now(),
                                                'fecha_atencion' => $fecha,
                                                'procedencia_hora' => 'AGENDAR_HORA'
                                            ]);

                                            $resp_ic_upd = Interconsulta::where('id', $ic_id)
                                                            ->update([
                                                                'estado_ic_id' => 10,
                                                                'usuario_id_mod' => $usuario,
                                                                'fecha_mod' => now()
                                            ]);

                                            $resp_cts_upd = CuentasCtes::where('ID', $resp_cts->id)
                                                                ->update([
                                                                    'interconsulta_id' => $ic_id
                                            ]);
                                            
                                        }

                                        if ($oa_id)
                                        {
                                            $resp_ic = DB::select( DB::raw(
                                                "SELECT 	A.ESTAB_ORIGEN_ID, 
                                                            A.ESTADO_IC_ID, 
                                                            A.FECHA_MOD, 
                                                            A.ID, 
                                                            A.USUARIO_ID_MOD
                                                    FROM 	INTERCONSULTA.INTERCONSULTA A
                                                    WHERE 	ID = :oa_id"), 
                                            array(
                                                'oa_id' => $oa_id
                                            ));

                                            $resp_mov_ic = MovEstadoIc::create([
                                                'ESTADO_IC_ID' => 10, 
                                                'FECHA_ESTADO' => now(), 
                                                'USUARIO_IC_ID' => $usuario, 
                                                'ESTABLECIMIENTO_ID' => $resp_ic[0]->estab_origen_id, 
                                                'OBSERVACION' => 'Proceso de asignación de hora', 
                                                'INTERCONSULTA_ID' => $oa_id
                                            ]); 

                                            $resp_ic = DB::update( DB::raw(
                                                "UPDATE INTERCONSULTA.INTERCONSULTA 
                                                        SET 	ESTADO_IC_ID = :estado_ic, 
                                                                USUARIO_ID_MOD = :usuario,
                                                                FECHA_MOD = :fecha,
                                                                HORAS_ASIGNADAS_ID = :id_horas_gen 
                                                        WHERE ID = :oa_id"), 
                                            array(
                                                'oa_id' => $oa_id,
                                                'id_horas_gen' => $id_hora_generada,
                                                'usuario' => $usuario,
                                                'fecha' => now(),
                                                'estado_ic' => 10
                                            ));


                                            $resp_cts = CuentasCtes::where('ID', $resp_cts->id)
                                                            ->update([
                                                                'oa_id' => $oa_id
                                                            ]);
                                        }
                                    
                                        $object_response['data'][0]['type'] = "Hora Generada";
                                        $object_response['data'][0]['id'] = $resp_hg->id;
                                        $object_response['data'][0]['attributes']['bloque_id'] = $bloque_id;
                                        $object_response['data'][0]['attributes']['fecha'] = $fecha;
                                        $object_response['data'][0]['attributes']['estado_horas_id'] = 7;
                                        $object_response['data'][0]['attributes']['tipo_atencion_e_id'] = $tipo_atencion_id;
                                        $object_response['data'][0]['attributes']['tipo_atencion_p_id'] = $tipo_atencion_id;
                                        $object_response['data'][0]['attributes']['tipo_solicitud_id'] = 7;
                                        $object_response['data'][0]['attributes']['fecha_creacion'] = now();
                                        $object_response['data'][0]['attributes']['fecha_asignada'] = now();
                                        $object_response['data'][0]['attributes']['usuario_id_mod'] = $usuario;
                                        $object_response['data'][0]['attributes']['interconsulta_id'] = @$ic_id;
                                        $object_response['data'][0]['attributes']['oa_id'] = @$oa_id;
                                        $object_response['data'][0]['attributes']['es_sobrecupo'] = '';
                                        throw new HttpResponseException(response()->json($object_response, 200));
                                    }

                                }else{
                                    $object_response['errors'][0]['status'] = 400;
                                    $object_response['errors'][0]['source'] = 'DarHoraController@agendarHora';
                                    $object_response['errors'][0]['title'] = 'No se agendo hora';
                                    $object_response['errors'][0]['detail'] = 'Paciente ingresado no posee ficha local';
                                    throw new HttpResponseException(response()->json($object_response, 400));
                                }
                            }
                        }

                    }else{
                        $object_response['errors'][0]['status'] = 400;
                        $object_response['errors'][0]['source'] = 'DarHoraController@agendarHora';
                        $object_response['errors'][0]['title'] = 'No se agendo hora';
                        $object_response['errors'][0]['detail'] = 'Tipo atencion no es valida para el establecimiento al que pertenece el bloque horario';
                        throw new HttpResponseException(response()->json($object_response, 400));
                        
                    }
                }
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DarHoraController@agendarHora';
                $object_response['errors'][0]['title'] = 'No se agendo hora';
                $object_response['errors'][0]['detail'] = 'El bloque horario ingresado se encuentra inactivo';
                throw new HttpResponseException(response()->json($object_response, 400));
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }           
    }
    
    /**
     * OBTENER DETALLE HORA AGENDADA
     */
    public function detalleHora(DetalleHoraRequest $request)
    {
        try {
            $resp_hora = HorasGeneradas::where('ID', $request->HORA_GENERADA_ID)->first();
            if ($resp_hora){            
                $object_response['data'][0]['type'] = "Detalle Hora Generada";
                $object_response['data'][0]['id'] = $resp_hora->id;
                $object_response['data'][0]['attributes']['bloque_horario_id'] = $resp_hora->bloque_horario_id;
                $object_response['data'][0]['attributes']['fecha'] = $resp_hora->fecha;
                $object_response['data'][0]['attributes']['estado_horas_id'] = $resp_hora->estado_horas_id;
                $object_response['data'][0]['attributes']['tipo_atencion_e_id'] = $resp_hora->tipo_atencion_e_id;
                $object_response['data'][0]['attributes']['tipo_atencion_p_id'] = $resp_hora->tipo_atencion_p_id;
                $object_response['data'][0]['attributes']['tipo_solicitud_id'] = $resp_hora->tipo_solicitud_id;
                $object_response['data'][0]['attributes']['fecha_creacion'] = $resp_hora->fecha_creacion;
                $object_response['data'][0]['attributes']['fecha_asignada'] = $resp_hora->fecha_asignada;
                $object_response['data'][0]['attributes']['usuario_id_mod'] = $resp_hora->usuario_id_mod;
                $object_response['data'][0]['attributes']['orden'] = $resp_hora->orden;
                $object_response['data'][0]['attributes']['ausencias_id'] = $resp_hora->ausencias_id;
                $object_response['data'][0]['attributes']['interconsulta_id'] = $resp_hora->interconsulta_id;
                $object_response['data'][0]['attributes']['oa_id'] = $resp_hora->oa_id;
                $object_response['data'][0]['attributes']['es_sobrecupo'] = $resp_hora->es_sobrecupo;
                $object_response['data'][0]['attributes']['visible'] = $resp_hora->visible;
                
                throw new HttpResponseException(response()->json($object_response, 200));   
            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DarHoraController@detalleHora';
                $object_response['errors'][0]['title'] = 'Consulta Detalle hora';
                $object_response['errors'][0]['detail'] = 'ID Hora consultada no existe';
                throw new HttpResponseException(response()->json($object_response, 400));   
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }    

}
