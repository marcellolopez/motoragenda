<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\ObtenerCuposRequest;
use App\Http\Requests\AsignarCantidadCuposRequest;
use App\Entities\Persona;
use App\Entities\Policlinico;
use App\Entities\BloqueHorario;
use App\Entities\DetalleBloqueHorario;
use App\Entities\JerarquiaEspec;
use App\Entities\AtencionEstab;
use Carbon\Carbon;

class DetallarAtencionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ObtenerCuposRequest $request)
    {
        try{         

            $bloque_horario_id =  $request->get('BLOQUE_HORARIO_ID') ;

            $bloque_horario = BloqueHorario::with('detallebloquehorario')
                ->where('ID', $bloque_horario_id)
                ->where('ACTIVO','S')
                ->first();
            
            if (!$bloque_horario) {
                $object_response["descripcion"] = "Bloque horario no se encuentra activo";

                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][0]['title'] =  'Bloque horario inactivo';
                $object_response['errors'][0]['detail'] = "Bloque horario no se encuentra activo";                
                throw new HttpResponseException(response()->json($object_response, 402));
            }            
            else{

                
                $desde =  Carbon::parse($bloque_horario->hora_inicio); 
                $hasta = Carbon::parse($bloque_horario->hora_fin);
                $diff_in_min = $hasta->diffInMinutes($desde);
                $total_cupos = intdiv($diff_in_min , $bloque_horario->tiempo_x_paciente);
                        
                
            }

            $object_response['data'][0]['id'] = $bloque_horario->id;
            $object_response['data'][0]['attributes']['hora_inicio'] = $bloque_horario->contrato_id;
            $object_response['data'][0]['attributes']['hora_fin'] = $bloque_horario->contrato_id;
            $object_response['data'][0]['attributes']['tiempo_x_paciente'] = $bloque_horario->tiempo_x_paciente;
            $object_response['data'][0]['attributes']['total_cupos'] = $total_cupos;
            throw new HttpResponseException(response()->json($object_response, 200));  
        }
        catch (Exception $e){
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function obtener_cupos($id)
    {
            $bloque_horario_id =  $request->get('BLOQUE_HORARIO_ID') ;

            $bloque_horario = BloqueHorario::with('detallebloquehorario')
                ->where('ID', $bloque_horario_id)
                ->where('ACTIVO','S')
                ->first();
            
            if (!$bloque_horario) {
                $object_response["descripcion"] = "Bloque horario no se encuentra activo";

                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DetalleAtencionController@index';
                $object_response['errors'][0]['title'] =  'Bloque horario inactivo';
                $object_response['errors'][0]['detail'] = "Bloque horario no se encuentra activo";                
                throw new HttpResponseException(response()->json($object_response, 402));
            }            
            else{

                
                $desde =  Carbon::parse($bloque_horario->hora_inicio); 
                $hasta = Carbon::parse($bloque_horario->hora_fin);
                $diff_in_min = $hasta->diffInMinutes($desde);
                $total_cupos = intdiv($diff_in_min , $bloque_horario->tiempo_x_paciente);
                        
                
            }
    }

    public function asignar_cantidad_cupos(AsignarCantidadCuposRequest $request)
    {
            $bloque_horario_id =  $request->get('BLOQUE_HORARIO_ID') ;

            $bloque_horario = BloqueHorario::with('detallebloquehorario')
                ->where('ID', $bloque_horario_id)
                ->where('ACTIVO','S')
                ->first();
            
            if (!$bloque_horario) {
                $object_response["descripcion"] = "Bloque horario no se encuentra activo";

                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'DetalleAtencionController@asignar_cantidad_cupos';
                $object_response['errors'][0]['title'] =  'Bloque horario inactivo';
                $object_response['errors'][0]['detail'] = "Bloque horario no se encuentra activo";                
                throw new HttpResponseException(response()->json($object_response, 402));
            }            
            else{
                $atencion_estab = AtencionEstab::where('TIPO_ATENCION_ID', $request->get('TIPO_ATENCION_ID'))
                    ->where('ESTABLECIMIENTO_ID',  $request->get('ESTABLECIMIENTO_ID'))
                    ->first();
                if (!$atencion_estab) {
                    $object_response["descripcion"] = "Tipo de atención no corresponde al establecimiento ingresado";

                    $object_response['errors'][0]['status'] = 400;
                    $object_response['errors'][0]['source'] = 'DetalleAtencionController@asignar_cantidad_cupos';
                    $object_response['errors'][0]['title'] =  '';
                    $object_response['errors'][0]['detail'] = "Tipo de atención no corresponde al establecimiento ingresado";                
                    throw new HttpResponseException(response()->json($object_response, 402));
                }   
                else{
                        $desde =  Carbon::parse($bloque_horario->hora_inicio); 
                        $hasta = Carbon::parse($bloque_horario->hora_fin);
                        $diff_in_min = $hasta->diffInMinutes($desde);
                        $total_cupos = intdiv($diff_in_min , $bloque_horario->tiempo_x_paciente);

                        $detallebloquehorario = DetalleBloqueHorario::where('bloque_horario_id', $request->get('BLOQUE_HORARIO_ID'))
                        ->where('ACTIVO','S')
                        ->get();

                        if (!$detallebloquehorario) {
                            $object_response["descripcion"] = "Detalle de bloque horario se encuentra inactivo";

                            $object_response['errors'][0]['status'] = 400;
                            $object_response['errors'][0]['source'] = 'DetalleAtencionController@asignar_cantidad_cupos';
                            $object_response['errors'][0]['title'] =  '';
                            $object_response['errors'][0]['detail'] = "Detalle de bloque horario se encuentra inactivo";                
                            throw new HttpResponseException(response()->json($object_response, 402));
                        }  
                        else{
                            
                            $suma = 0;
                            foreach ($detallebloquehorario as $d) {
                                $suma = $suma + $d->cantidad;
                            }

                            $cupos_disponibles = $total_cupos - $suma;

                            if($cupos_disponibles >= $request->cantidad)
                            {

                                $detallebloquehorario_existe = DetalleBloqueHorario::where('bloque_horario_id', $request->get('BLOQUE_HORARIO_ID'))
                                ->where('tipo_atencion_id', $request->get('TIPO_ATENCION_ID'))
                                ->count();
                                if($detallebloquehorario_existe > 0){
                                    $object_response["descripcion"] = "Detalle de bloque horario se encuentra ingresado";

                                    $object_response['errors'][0]['status'] = 400;
                                    $object_response['errors'][0]['source'] = 'DetalleAtencionController@asignar_cantidad_cupos';
                                    $object_response['errors'][0]['title'] =  '';
                                    $object_response['errors'][0]['detail'] = "Detalle de bloque horario se encuentra ingresado";                
                                    throw new HttpResponseException(response()->json($object_response, 402));
                                }
                                else{
                                    $detallebloquehorario = DetalleBloqueHorario::create([
                                        'BLOQUE_HORARIO_ID' => $request->get('BLOQUE_HORARIO_ID'),
                                        'TIPO_ATENCION_ID' => $request->get('TIPO_ATENCION_ID'),
                                        'CANTIDAD' =>  $request->get('CANTIDAD'),
                                        'ACTIVO' =>  'S',
                                        'USUARIO_CREA_ID' => $request->get('USUARIO_CREA_ID'),
                                        'FECHA_CREA' => now(),
                                        'IP_CREA' => $request->get('IP_CREA'),                      
                                    ]);

                                    throw new HttpResponseException(response()->json($detallebloquehorario, 402));
                                }
                            }
                            else{
                                $object_response["descripcion"] = "Detalle de bloque horario se encuentra inactivo";

                                $object_response['errors'][0]['status'] = 400;
                                $object_response['errors'][0]['source'] = 'DetalleAtencionController@asignar_cantidad_cupos';
                                $object_response['errors'][0]['title'] =  '';
                                $object_response['errors'][0]['detail'] = "Detalle de bloque horario se encuentra inactivo";                
                                throw new HttpResponseException(response()->json($object_response, 402));
                            }
                        } 
                }
            }        
    }
}
