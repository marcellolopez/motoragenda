<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Carbon\Carbon;
use App\Http\Controllers\Helpers\Format;

use App\Entities\BloqueHorario;
use App\Entities\JerarquiaEspec;

use App\Http\Requests\CrearBloqueHorarioRequest;
use App\Http\Requests\ListarBloqueHorarioRequest;
use App\Http\Requests\ActualizaBloqueHorarioRequest;

class BloqueHorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListarBloqueHorarioRequest $object_request)
    {
        try
        {
            $result_bh = BloqueHorario::with('jerarquia_espec')
                                    ->where('CONTRATO_ID','=',$object_request->CONTRATO_ID)
                                    ->where('ACTIVO','=','S')
                                    ->where('DIA','<>',0)
                                    ->orderBy("DIA", "ASC")
                                    ->get();
                                    
            for($i = 0; $i < count($result_bh); $i++){
                $object_response['data'][$i]['type'] = "Bloque_Horario";
                $object_response['data'][$i]['id'] = $result_bh[$i]->id;
                $object_response['data'][$i]['attributes']['contrato_id'] = $result_bh[$i]->contrato_id;
                $object_response['data'][$i]['attributes']['jerarquia_espec_id'] = $result_bh[$i]->jerarquia_espec_id;
                $object_response['data'][$i]['attributes']['policlinico'] = $result_bh[$i]->jerarquia_espec->policlinico->descripcion;
                $object_response['data'][$i]['attributes']['dia'] = Format::nombreDias($result_bh[$i]->dia);
                $object_response['data'][$i]['attributes']['tiempo_x_paciente'] = $result_bh[$i]->tiempo_x_paciente;
                $object_response['data'][$i]['attributes']['hora_inicio'] = $result_bh[$i]->hora_inicio;
                $object_response['data'][$i]['attributes']['hora_fin'] = $result_bh[$i]->hora_fin;
                $object_response['data'][$i]['attributes']['tipo_bloque_id'] = $result_bh[$i]->tipo_bloque_id;
                $object_response['data'][$i]['attributes']['activo'] = $result_bh[$i]->activo;
                $object_response['data'][$i]['attributes']['usuario_mod_id'] = $result_bh[$i]->usuario_mod_id;
                $object_response['data'][$i]['attributes']['fecha_mod'] = $result_bh[$i]->fecha_mod;
                $object_response['data'][$i]['attributes']['ausencias_id'] = $result_bh[$i]->ausencias_id;
            }
            throw new HttpResponseException(response()->json($object_response, 200));  
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CrearBloqueHorarioRequest $object_request)
    {
        try
        {
            $flg_ingreso_bh = false;
            $tiempo_x_paciente = 0;
            if (@!$object_request->ACTIVO)
            {
                $object_request->ACTIVO = "S";
            }
            if (@!$object_request->AUSENCIAS_ID)
            {
                $object_request->AUSENCIAS_ID = NULL;
            }
            $result_bh = BloqueHorario::where('CONTRATO_ID','=',$object_request->CONTRATO_ID)
                                      ->where('ACTIVO','=','S')
                                      ->where('DIA','<>',0)
                                      ->get();

            if ($result_bh->count() > 0)
            {
                //VALIDACION SI EXISTE BLOQUE HORARIO PARA EL DIA Y CONTRATO INGRESADOS
                for($i = 0; $i < $result_bh->count(); $i++)
                {                   
                    if ($result_bh[$i]->dia == $object_request->DIA)
                    {
                        $flg_ingreso_bh = false;
                        $object_response["success"] = false;
                        $object_response["message"] = "Ya existe un bloque horario para el contrato y dia ingresado.";                
        
                        $object_response["data"][0]['id_contrato'] = $object_request->CONTRATO_ID;  
                        $object_response["data"][0]['id_bloque_horario'] = $result_bh[$i]->id;
                        $object_response["data"][0]['dia'] = Format::nombreDias($result_bh[$i]->dia);
                        $object_response["data"][0]['hora_inicio'] = $result_bh[$i]->hora_inicio;
                        $object_response["data"][0]['hora_fin'] = $result_bh[$i]->hora_fin;
                        $object_response["data"][0]['tiempo_x_paciente'] = $result_bh[$i]->tiempo_x_paciente;
            
                        throw new HttpResponseException(response()->json($object_response, 200));    
                    }
                    else
                    {
                        $flg_ingreso_bh = true;
                    }              
                }
            }
            else
            {
                $flg_ingreso_bh = true;
            }

            //GUARDA BLOQUE HORARIO
            if ($flg_ingreso_bh)
            {
                //CALCULO DE TIEMPO POR PACIENTE CUANDO ES BLOQUE HORARIO POR PERIODO
                if (@$object_request->CANTIDAD_PACIENTES)
                {
                    $desde =  Carbon::parse($object_request->HORA_INICIO); 
                    $hasta = Carbon::parse($object_request->HORA_FIN);

                    $diff_in_min = $hasta->diffInMinutes($desde);
                    $tiempo_x_paciente = intdiv($diff_in_min , $object_request->CANTIDAD_PACIENTES);

                }else
                {
                    $tiempo_x_paciente = $object_request->TIEMPO_X_PACIENTE;
                }

                /* CREACION BLOQUE HORARIO Y RESPONSE */
                    $response_bh = BloqueHorario::create([
                        'CONTRATO_ID' => $object_request->CONTRATO_ID,
                        'JERARQUIA_ESPEC_ID' => $object_request->JERARQUIA_ESPEC_ID,
                        'DIA' => $object_request->DIA,
                        'TIEMPO_X_PACIENTE' => $tiempo_x_paciente,
                        'HORA_INICIO' => $object_request->HORA_INICIO,
                        'HORA_FIN' => $object_request->HORA_FIN,
                        'TIPO_BLOQUE_ID' => $object_request->TIPO_BLOQUE_ID,
                        'ACTIVO' => $object_request->ACTIVO,
                        'USUARIO_MOD_ID' => $object_request->USUARIO_MOD_ID,
                        'FECHA_MOD' => now(),
                        'AUSENCIAS_ID' => $object_request->AUSENCIAS_ID
                    ]);
                    $object_response["success"] = true;
                    $object_response["message"] = "Bloque horario ingresado con exito";
                    $object_response["data"][0]['id'] = $response_bh->id;
                    $object_response["data"][0]['attributes']['contrato_id'] = $object_request->CONTRATO_ID;
                    $object_response["data"][0]['attributes']['jerarquia_espec_id'] = $object_request->JERARQUIA_ESPEC_ID;
                    $object_response["data"][0]['attributes']['dia'] = Format::nombreDias($object_request->DIA);
                    $object_response["data"][0]['attributes']['tiempo_x_paciente'] = $tiempo_x_paciente;
                    $object_response["data"][0]['attributes']['hora_inicio'] = $object_request->HORA_INICIO;
                    $object_response["data"][0]['attributes']['hora_fin'] = $object_request->HORA_FIN;
                    $object_response["data"][0]['attributes']['tipo_bloque_id'] = $object_request->TIPO_BLOQUE_ID;
                    $object_response["data"][0]['attributes']['activo'] = $object_request->ACTIVO;
                    $object_response["data"][0]['attributes']['usuario_mod_id'] = $object_request->USUARIO_MOD_ID;
                    $object_response["data"][0]['attributes']['fecha_mod'] = now();
                    $object_response["data"][0]['attributes']['ausencia_id'] = $object_request->AUSENCIAS_ID;
                    
                    throw new HttpResponseException(response()->json($object_response, 200));
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActualizaBloqueHorarioRequest $object_request, $id)
    {
<<<<<<< HEAD
        try
        {    
            $bloque_horario = BloqueHorario::with('jerarquia_espec')
                                        ->where('ID', $id)
                                        ->where('ACTIVO','S')
                                        ->where('DIA', '<>', 0)
                                        ->first();
                                
=======
        try{
            $bloque_horario = BloqueHorario::with('jerarquia_espec')
                                           ->where('ID', $id)
                                           ->where('ACTIVO','S')
                                           ->where('DIA', '<>', 0)
                                           ->first();
                                   
>>>>>>> 5d2b02b4d3775f1ef6b5b09e9299e9083bb2de8b

            if ($bloque_horario){
                $response_bh = BloqueHorario::where('ID', $id)
                                            ->update([
                                                'jerarquia_espec_id' => $object_request->JERARQUIA_ESPEC_ID,
                                                'usuario_mod_id' => $object_request->USUARIO_MOD_ID,
                                                'fecha_mod' => now()
                ]);
                                                
                $jerarquia_espec = JerarquiaEspec::with('policlinico')
                                ->where('ID', $object_request->JERARQUIA_ESPEC_ID)
                                ->first();

                $object_response["success"] = true;
                $object_response["message"] = "Bloque horario actualizado con exito";
                $object_response["data"][0]['id'] = $bloque_horario->id;
                $object_response["data"][0]['attributes']['contrato_id'] = $bloque_horario->contrato_id;
                $object_response["data"][0]['attributes']['jerarquia_espec_id'] = $object_request->JERARQUIA_ESPEC_ID;
                $object_response["data"][0]['attributes']['policlinico'] = $jerarquia_espec->policlinico->descripcion;
                $object_response["data"][0]['attributes']['dia'] = Format::nombreDias($bloque_horario->dia);
                $object_response["data"][0]['attributes']['tiempo_x_paciente'] = $bloque_horario->tiempo_x_paciente;
                $object_response["data"][0]['attributes']['hora_inicio'] = $bloque_horario->hora_inicio;
                $object_response["data"][0]['attributes']['hora_fin'] = $bloque_horario->hora_fin;
                $object_response["data"][0]['attributes']['tipo_bloque_id'] = $bloque_horario->tipo_bloque_id;
                $object_response["data"][0]['attributes']['activo'] = $bloque_horario->activo;
                $object_response["data"][0]['attributes']['usuario_mod_id'] = $object_request->USUARIO_MOD_ID;
                $object_response["data"][0]['attributes']['fecha_mod'] = now();
                $object_response["data"][0]['attributes']['ausencia_id'] = $bloque_horario->ausencias_id;
                
                throw new HttpResponseException(response()->json($object_response, 200));


            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'BloqueHorarioController@update';
                $object_response['errors'][0]['title'] = 'ID de bloque horario';
                $object_response['errors'][0]['detail'] = 'No se encontro el bloque horario activo de ID: '.$id;
                throw new HttpResponseException(response()->json($object_response, 400));    
            }
<<<<<<< HEAD
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
=======
>>>>>>> 5d2b02b4d3775f1ef6b5b09e9299e9083bb2de8b
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $bloque_horario = BloqueHorario::with('jerarquia_espec')
                                        ->where('ID', $id)
                                        ->where('ACTIVO','S')
                                        ->where('DIA', '<>', 0)
                                        ->first();
            if ($bloque_horario){
                $response_bh = BloqueHorario::where('ID', $id)
                                            ->update([
                                                'activo' => 'N',
                                                'fecha_mod' => now()
                ]);
                                                
                $jerarquia_espec = JerarquiaEspec::with('policlinico')
                                ->where('ID', $bloque_horario->jerarquia_espec_id)
                                ->first();

                $object_response["success"] = true;
                $object_response["message"] = "Bloque horario eliminado (desactivado) con exito";
                $object_response["data"][0]['id'] = $id;
                $object_response["data"][0]['attributes']['contrato_id'] = $bloque_horario->contrato_id;
                $object_response["data"][0]['attributes']['jerarquia_espec_id'] = $bloque_horario->jerarquia_espec_id;
                $object_response["data"][0]['attributes']['policlinico'] = $jerarquia_espec->policlinico->descripcion;
                $object_response["data"][0]['attributes']['dia'] = Format::nombreDias($bloque_horario->dia);
                $object_response["data"][0]['attributes']['tiempo_x_paciente'] = $bloque_horario->tiempo_x_paciente;
                $object_response["data"][0]['attributes']['hora_inicio'] = $bloque_horario->hora_inicio;
                $object_response["data"][0]['attributes']['hora_fin'] = $bloque_horario->hora_fin;
                $object_response["data"][0]['attributes']['tipo_bloque_id'] = $bloque_horario->tipo_bloque_id;
                $object_response["data"][0]['attributes']['activo'] = $bloque_horario->activo;
                $object_response["data"][0]['attributes']['usuario_mod_id'] = $bloque_horario->usuario_mod_id;
                $object_response["data"][0]['attributes']['fecha_mod'] = now();
                $object_response["data"][0]['attributes']['ausencia_id'] = $bloque_horario->ausencias_id;
                
                throw new HttpResponseException(response()->json($object_response, 200));


            }else{
                $object_response['errors'][0]['status'] = 400;
                $object_response['errors'][0]['source'] = 'BloqueHorarioController@destroy';
                $object_response['errors'][0]['title'] = 'ID de bloque horario';
                $object_response['errors'][0]['detail'] = 'No se encontro el bloque horario activo de ID: '.$id;
                throw new HttpResponseException(response()->json($object_response, 400));    
            }
        }
        catch (Exception $e)
        {
            $object_response["descripcion"] = $e;
            throw new HttpResponseException(response()->json($object_response, 402));
        }
    }
}