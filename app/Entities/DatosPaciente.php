<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DatosPaciente extends Model
{
    public $timestamps = false;
    protected $table = 'REFCENTRAL.DATOS_PACIENTE';
    protected $connection = 'oracle';
    protected $guarded = ['ID'];
   // public $fillable = [];
    const UPDATED_AT = 'FECHA_MOD';

}
