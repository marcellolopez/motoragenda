<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SolicitudBloqueo extends Model
{
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'SOLICIT_BLOQUEO';
    protected $connection = 'oracle';
    //public $fillable = [];
}
