<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MotivoAusencia extends Model
{
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'MOTIVO_AUSENCIA';
    protected $connection = 'oracle';
    public $fillable = [
		'CODIGO',
		'DESCRIPCION',
		'ACTIVO',
		'USUARIO_MOD_ID',
		'FECHA_MOD'
    ];
}
