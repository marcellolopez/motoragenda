<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DestinoPaciente extends Model
{
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'DESTINO_PACIENTE';
    protected $connection = 'oracle';
    //public $fillable = [];
}
