<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PeriodoProgram extends Model
{
    //public $incrementing = false;
    protected $table = 'PERIODO_PROGRAM';
    protected $guarded = ['ID'];
    protected $connection = 'oracle';
    public $fillable = ['ESTABLECIMIENTO_ID', 'FECHA_DESDE', 'FECHA_HASTA', 'ACTIVO', 'USUARIO_ID_MOD', 'FECHA_MOD'];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';
}
