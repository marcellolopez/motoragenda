<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MovEstadoIc extends Model
{
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'MOV_ESTADO_IC';
    protected $connection = 'oracle';
    //public $fillable = [];
}
