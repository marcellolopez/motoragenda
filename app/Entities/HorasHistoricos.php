<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HorasHistoricos extends Model
{
    protected $guarded = ['ID'];
    protected $table = 'HORAS_HISTORICOS';
    protected $connection = 'oracle';
    // public $fillable = ['BLOQUE_HORARIO_ID', 'FECHA', 'ESTADO_HORAS_ID', 'TIPO_ATENCION_E_ID', 'TIPO_ATENCION_P_ID', 'TIPO_SOLICITUD_ID', 'FECHA_CREACION', 
    //                   'FECHA_ASIGNADA', 'USUARIO_ID_MOD', 'ORDEN', 'AUSENCIAS_ID', 'INTERCONSULTA_ID', 'OA_ID', 'ES_SOBRECUPO', 'VISIBLE'];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';
}
