<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class InterconsultaHoras extends Model
{
    //public $incrementing = false;
    protected $guarded = ['ID'];
    protected $table = 'DBGES.INTERCONSULTA_HORAS';
    protected $connection = 'oracle';
    //public $fillable = [];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';
}
