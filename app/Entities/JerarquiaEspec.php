<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class JerarquiaEspec extends Model
{
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'REFCENTRAL.JERARQUIA_ESPEC';
    protected $connection = 'oracle';
    //public $fillable = ['', '', '', '', '', ''];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';    

    public function policlinico()
    {
        return $this->belongsTo('App\Entities\Policlinicos', 'policlinicos_id');
    }

}

