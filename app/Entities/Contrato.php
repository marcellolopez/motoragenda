<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table = 'contrato';

    public $fillable = [
		'ESTABLECIMIENTO_ID',
		'PERSONA_ID',
		'RESOLUCION',
		'FECHA_RESOLUCION',
		'FECHA_INICIO',
		'FECHA_FIN',
		'VIGENTE',
		'HORAS',
		'CALIDADJURIDICA_ID',
		'CARGO_ID',
		'ACTIVO',
		'USUARIO_ID_MOD',
		'FECHA_MOD',
		'ESPECIALIDADES_ID',
		'SUBESPECIALIDAD_ID'
    ];    

	public function establecimiento()
	{
	    return $this->belongsTo('\App\Entities\Establecimiento', 'establecimiento_id', 'id');
	}    

	public function cargo()
	{
	    return $this->belongsTo('\App\Entities\Cargo', 'cargo_id', 'id');
	}    

    public function ausencia()
    {
        return $this->hasMany('App\Entities\Ausencia', 'contrato_id');
    }  

    public function persona()
    {
        return $this->belongsTo('App\Entities\Persona', 'persona_id');
    }  

	public function motivoausencia()
	{
	    return $this->belongsTo('\App\Entities\MotivoAusencia', 'App\Entities\Ausencia', 'motivo_ausencia_id', 'id');
	}        
}
