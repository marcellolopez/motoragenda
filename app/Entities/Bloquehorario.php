<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BloqueHorario extends Model
{
    public $timestamps = false;
        protected $table = 'BLOQUE_HORARIO';
    protected $connection = 'oracle';
    protected $guarded = ['ID'];
    public $fillable = ['CONTRATO_ID', 'JERARQUIA_ESPEC_ID', 'DIA', 'TIEMPO_X_PACIENTE', 'HORA_INICIO', 'HORA_FIN', 'TIPO_BLOQUE_ID', 'ACTIVO', 'USUARIO_MOD_ID', 'FECHA_MOD', 'AUSENCIAS_ID'];
    const UPDATED_AT = 'FECHA_MOD';


    public function jerarquia_espec()
    {
        return $this->belongsTo('App\Entities\JerarquiaEspec', 'jerarquia_espec_id');
    }
    public function detallebloquehorario()
    {
        return $this->hasMany('App\Entities\DetalleBloqueHorario', 'bloque_horario_id');
    }    

    public function contrato()
    {
        return $this->belongsTo('\App\Entities\Contrato', 'contrato_id', 'id');
    }   

}

