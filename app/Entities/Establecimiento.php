<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Establecimiento extends Model{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = 'REFCENTRAL.ESTABLECIMIENTO';
    protected $connection = 'oracle';
    
}