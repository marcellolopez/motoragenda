<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Feriado extends Model
{
    protected $table = 'FERIADOS';
    protected $connection = 'oracle';
    protected $guarded = ['ID'];
    public $fillable = ['ESTABLECIMIENTO_ID', 'FECHA_DESDE', 'FECHA_HASTA', 'ACTIVO', 'USUARIO_MOD_ID', 'FECHA_MOD','IP_MOD','USUARIO_CREA_ID', 'FECHA_CREA', 'IP_CREA'];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';

}
