<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HorasGeneradas extends Model
{
    //public $incrementing = false;
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'HORAS_GENERADAS';
    protected $connection = 'oracle';
    public $fillable = ['BLOQUE_HORARIO_ID', 'FECHA', 'ESTADO_HORAS_ID', 'TIPO_ATENCION_E_ID', 'TIPO_ATENCION_P_ID', 'TIPO_SOLICITUD_ID', 'FECHA_CREACION', 
                        'FECHA_ASIGNADA', 'USUARIO_ID_MOD', 'ORDEN', 'AUSENCIAS_ID', 'INTERCONSULTA_ID', 'OA_ID', 'ES_SOBRECUPO', 'VISIBLE'];
<<<<<<< HEAD
=======

    //const CREATED_AT = 'FECHA_MOD';
    //const UPDATED_AT = 'FECHA_MOD';
>>>>>>> 5d2b02b4d3775f1ef6b5b09e9299e9083bb2de8b
    const CREATED_AT = 'FECHA_CREACION';
    const UPDATED_AT = 'FECHA_ASIGNADA';
}
