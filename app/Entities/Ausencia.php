<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Ausencia extends Model
{
    public $timestamps = false;
    protected $guarded = ['ID'];
    protected $table = 'REFCENTRAL.AUSENCIAS';
    protected $connection = 'oracle';
	public $fillable = [
		'CONTRATO_ID',
		'FECHA',
		'FECHA_DESDE',
		'FECHA_HASTA',
		'MOTIVO_AUSENCIA_ID',
		'CANTIDAD_PACIENTES',
		'DESTINO_PACIENT_ID',
		'SOLICITANT_BLOQ_ID',
		'OBSERVACION',
		'ACTIVO',
		'USUARIO_ID_MOD',
		'FECHA_MOD',
		'BLOQUE_HORARIO_ID',
		'USUARIO_CREA_ID',
		'FECHA_CREA',
		'USUARIO_MOD_ID',
		'IP_CREA',
		'IP_MOD'
	];

	public function contrato()
	{
	    return $this->belongsTo('\App\Entities\Contrato', 'contrato_id', 'id');
	}   
	public function motivoausencia()
	{
	    return $this->belongsTo('\App\Entities\MotivoAusencia', 'motivo_ausencia_id', 'id');
	}  
	public function destinopaciente()
	{
	    return $this->belongsTo('\App\Entities\DestinoPaciente', 'destino_pacient_id', 'id');
	}   
	public function solicitudbloqueo()
	{
	    return $this->belongsTo('\App\Entities\SolicitudBloqueo', 'solicitant_bloq_id', 'id');
	}   
    
}

