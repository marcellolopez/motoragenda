<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AtencionEstab extends Model
{
    protected $table = 'ATENCION_ESTAB';

	public function establecimiento()
	{
	    return $this->belongsTo('\App\Entities\Establecimiento', 'establecimiento_id', 'id');
	} 

	public function tipo_atencion()
	{
	    return $this->belongsTo('\App\Entities\TipoAtencion', 'tipo_atencion_id', 'id');
	} 	    
    
}
