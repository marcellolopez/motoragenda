<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Usuario extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = 'REFCENTRAL.USUARIO';
    protected $connection = 'oracle';

}
