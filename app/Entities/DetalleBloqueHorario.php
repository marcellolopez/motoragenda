<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DetalleBloqueHorario extends Model
{
    protected $table = 'DETALLE_BLOQ_HOR';
    protected $connection = 'oracle';
    public $timestamps = false; 
	public $fillable = [
		'BLOQUE_HORARIO_ID',
		'TIPO_ATENCION_ID',
		'CANTIDAD',
		'ACTIVO',
		'USUARIO_CREA_ID',
		'FECHA_CREA',
		'IP_CREA',
		'USUARIO_MOD_ID',
		'FECHA_MOD'
	];

	public function bloquehorario()
	{
	    return $this->belongsTo('\App\Entities\Bloquehorario', 'bloque_horario_id', 'id');
	}
	public function tipoatencion()
	{
	    return $this->belongsTo('\App\Entities\TipoAtencion', 'tipo_atencion_id', 'id');
	}
}
