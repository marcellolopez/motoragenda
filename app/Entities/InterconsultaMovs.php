<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class InterconsultaMovs extends Model
{
    protected $guarded = ['ID'];
    protected $table = 'DBGES.INTERCONSULTA_MOVS';
    protected $connection = 'oracle';
   // public $fillable = ['INTERCONSULTA_ID','FECHA','ESTAB_DESTINO_ID','JERARQUIA_DEST_ID','TIPO_DERIVACION_ID','ESTADO_IC_ID','ESTADO_AUGE_ID','ALERTA_ID',
    //                    'DIAG_CIEL0_1_ID','DIAG_CIEL0_2_ID','RGS_ID','ESTAB_ORIGEN_ID','MOTIVO_INTERCONSULTA_ID','USUARIO_ID_MOD','OBSERVACION','ESTADO_IFL_ID','PROCEDENCIA_ID','N_DOCUMENTO_PROCEDENCIA'];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';
    
}
