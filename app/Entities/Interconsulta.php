<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Interconsulta extends Model
{
    protected $guarded = ['ID'];
    protected $table = 'DBGES.INTERCONSULTA';
    protected $connection = 'oracle';
    //public $fillable = [];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_MOD';
}
