<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DatosLocales extends Model
{
    public $timestamps = false;
    protected $table = 'REFCENTRAL.DATOS_LOCALES';
    protected $connection = 'oracle';
    protected $guarded = ['ID'];
   // public $fillable = [];
    const CREATED_AT = 'FECHA_MOD';
    const UPDATED_AT = 'FECHA_ULT_ACT';

	public function establecimiento()
	{
	    return $this->belongsTo('\App\Entities\Establecimiento', 'establecimiento_id', 'id');
	}    
}
