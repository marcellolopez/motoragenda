<?php
/** 
 * Desarrollador: Israel Jensen / IngeniaGlobal
 * 02/12/2019
 */

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('Programacion', 'PeriodoProgramController');
Route::resource('Bloquehorario', 'BloqueHorarioController');
Route::resource('Feriados', 'FeriadoController');
    Route::put('FeriadoDelete', 'FeriadoController@eliminar'); //provisorio, inconveniente con recurso en resourse controller
    Route::get('VerificaFeriado', 'FeriadoController@verificaFeriado');
Route::resource('DetallarAtencion', 'DetallarAtencionController');
Route::resource('SobreCupo', 'SobreCupoController');
    Route::get('buscarBloquesHorarios', 'SobreCupoController@buscarBloquesHorarios');
    Route::get('obtenerProfesional'   , 'SobreCupoController@index');
    Route::get('asignarCantidadCupos'   , 'DetallarAtencionController@asignar_cantidad_cupos');
    Route::get('listadoSobrecuposPaciente'   , 'SobreCupoController@listadoSobrecuposPaciente');
Route::post('bloquesHorarioProfesional', 'HorasReasignarController@bloquesHorarioProfesional');

Route::post('obtenerProfesionalCantidadBloqueos', 'HorasReasignarController@obtenerProfesionalCantidadBloqueos');
    Route::get('obtenerContratosProfesional'   , 'BloquearAgendaController@index');
    Route::get('obtenerMotivosAusencia'   , 'BloquearAgendaController@obtenerMotivosAusencia');
    Route::get('insertarBloqueo'   , 'BloquearAgendaController@insertarBloqueo');
    Route::get('obtenerBloqueos'   , 'DesbloquearAgendaController@obtenerBloqueos');
    Route::get('eliminarBloqueo'   , 'DesbloquearAgendaController@eliminarBloqueo');

    Route::get('verificarBloqueo'   , 'BloquearAgendaController@verificarBloqueo');



Route::post('listadoBloqueosProfesional', 'HorasReasignarController@listadoBloqueosProfesional');
Route::post('horasReasignardeBloqueHorario', 'HorasReasignarController@horasReasignardeBloqueHorario');

Route::post('busquedaRut', 'DarHoraController@busquedaRut');
Route::post('busquedaNombre', 'DarHoraController@busquedaNombre');
Route::post('busquedaFicha', 'DarHoraController@busquedaFicha');
Route::post('listadoPoliclinicos', 'DarHoraController@listadoPoliclinicos');
Route::post('listadoProfesionales', 'DarHoraController@listadoProfesionales');
Route::post('listadoCuposDisponibles', 'DarHoraController@listadoCuposDisponibles');
Route::post('listadoHorasPorFecha', 'DarHoraController@listadoHorasPorFecha');
Route::post('agendarHora', 'DarHoraController@agendarHora');
Route::post('detalleHora', 'DarHoraController@detalleHora');

